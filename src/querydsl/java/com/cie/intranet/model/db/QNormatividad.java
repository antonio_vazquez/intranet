package com.cie.intranet.model.db;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QNormatividad is a Querydsl query type for Normatividad
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QNormatividad extends EntityPathBase<Normatividad> {

    private static final long serialVersionUID = -814105070L;

    public static final QNormatividad normatividad = new QNormatividad("normatividad");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath nombre = createString("nombre");

    public QNormatividad(String variable) {
        super(Normatividad.class, forVariable(variable));
    }

    public QNormatividad(Path<? extends Normatividad> path) {
        super(path.getType(), path.getMetadata());
    }

    public QNormatividad(PathMetadata metadata) {
        super(Normatividad.class, metadata);
    }

}

