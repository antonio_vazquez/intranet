package com.cie.intranet.model.db;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QUsuarioPlanta is a Querydsl query type for UsuarioPlanta
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QUsuarioPlanta extends EntityPathBase<UsuarioPlanta> {

    private static final long serialVersionUID = 1844141036L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QUsuarioPlanta usuarioPlanta = new QUsuarioPlanta("usuarioPlanta");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final QPlanta planta;

    public final QUsuario usuario;

    public QUsuarioPlanta(String variable) {
        this(UsuarioPlanta.class, forVariable(variable), INITS);
    }

    public QUsuarioPlanta(Path<? extends UsuarioPlanta> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QUsuarioPlanta(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QUsuarioPlanta(PathMetadata metadata, PathInits inits) {
        this(UsuarioPlanta.class, metadata, inits);
    }

    public QUsuarioPlanta(Class<? extends UsuarioPlanta> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.planta = inits.isInitialized("planta") ? new QPlanta(forProperty("planta"), inits.get("planta")) : null;
        this.usuario = inits.isInitialized("usuario") ? new QUsuario(forProperty("usuario"), inits.get("usuario")) : null;
    }

}

