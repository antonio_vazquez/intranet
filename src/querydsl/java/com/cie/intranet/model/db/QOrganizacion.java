package com.cie.intranet.model.db;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QOrganizacion is a Querydsl query type for Organizacion
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QOrganizacion extends EntityPathBase<Organizacion> {

    private static final long serialVersionUID = 1071576604L;

    public static final QOrganizacion organizacion = new QOrganizacion("organizacion");

    public final StringPath compania = createString("compania");

    public final StringPath correo = createString("correo");

    public final StringPath departamento = createString("departamento");

    public final StringPath descripcion = createString("descripcion");

    public final StringPath dn = createString("dn");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath titulo = createString("titulo");

    public QOrganizacion(String variable) {
        super(Organizacion.class, forVariable(variable));
    }

    public QOrganizacion(Path<? extends Organizacion> path) {
        super(path.getType(), path.getMetadata());
    }

    public QOrganizacion(PathMetadata metadata) {
        super(Organizacion.class, metadata);
    }

}

