package com.cie.intranet.model.db;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QConfiguracion is a Querydsl query type for Configuracion
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QConfiguracion extends EntityPathBase<Configuracion> {

    private static final long serialVersionUID = -1379770769L;

    public static final QConfiguracion configuracion = new QConfiguracion("configuracion");

    public final StringPath comentario = createString("comentario");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath parametro = createString("parametro");

    public final StringPath valor = createString("valor");

    public QConfiguracion(String variable) {
        super(Configuracion.class, forVariable(variable));
    }

    public QConfiguracion(Path<? extends Configuracion> path) {
        super(path.getType(), path.getMetadata());
    }

    public QConfiguracion(PathMetadata metadata) {
        super(Configuracion.class, metadata);
    }

}

