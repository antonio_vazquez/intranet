package com.cie.intranet.model.db;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QProcedimiento is a Querydsl query type for Procedimiento
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProcedimiento extends EntityPathBase<Procedimiento> {

    private static final long serialVersionUID = -1913621028L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QProcedimiento procedimiento = new QProcedimiento("procedimiento");

    public final StringPath codigo = createString("codigo");

    public final StringPath descripcion = createString("descripcion");

    public final StringPath extension = createString("extension");

    public final StringPath filename = createString("filename");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath nombre = createString("nombre");

    public final ArrayPath<byte[], Byte> pdf = createArray("pdf", byte[].class);

    public final DateTimePath<java.util.Date> register = createDateTime("register", java.util.Date.class);

    public final QUsuario usuario;

    public QProcedimiento(String variable) {
        this(Procedimiento.class, forVariable(variable), INITS);
    }

    public QProcedimiento(Path<? extends Procedimiento> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QProcedimiento(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QProcedimiento(PathMetadata metadata, PathInits inits) {
        this(Procedimiento.class, metadata, inits);
    }

    public QProcedimiento(Class<? extends Procedimiento> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.usuario = inits.isInitialized("usuario") ? new QUsuario(forProperty("usuario"), inits.get("usuario")) : null;
    }

}

