package com.cie.intranet.model.db;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QDistribucion is a Querydsl query type for Distribucion
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDistribucion extends EntityPathBase<Distribucion> {

    private static final long serialVersionUID = -1688053907L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QDistribucion distribucion1 = new QDistribucion("distribucion1");

    public final QComunicado comunicado;

    public final StringPath distribucion = createString("distribucion");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public QDistribucion(String variable) {
        this(Distribucion.class, forVariable(variable), INITS);
    }

    public QDistribucion(Path<? extends Distribucion> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QDistribucion(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QDistribucion(PathMetadata metadata, PathInits inits) {
        this(Distribucion.class, metadata, inits);
    }

    public QDistribucion(Class<? extends Distribucion> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.comunicado = inits.isInitialized("comunicado") ? new QComunicado(forProperty("comunicado"), inits.get("comunicado")) : null;
    }

}

