package com.cie.intranet.model.db;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QPolitica is a Querydsl query type for Politica
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPolitica extends EntityPathBase<Politica> {

    private static final long serialVersionUID = 1549060743L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPolitica politica = new QPolitica("politica");

    public final QArea area;

    public final StringPath codigo = createString("codigo");

    public final StringPath descripcion = createString("descripcion");

    public final StringPath estatus = createString("estatus");

    public final StringPath extension = createString("extension");

    public final StringPath filename = createString("filename");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final QIntranetFile intranetFile;

    public final StringPath nombre = createString("nombre");

    public final QNormatividad normatividad;

    public final ArrayPath<byte[], Byte> pdf = createArray("pdf", byte[].class);

    public final DateTimePath<java.util.Date> register = createDateTime("register", java.util.Date.class);

    public final QSubarea subarea;

    public final StringPath username = createString("username");

    public final QUsuario usuario;

    public QPolitica(String variable) {
        this(Politica.class, forVariable(variable), INITS);
    }

    public QPolitica(Path<? extends Politica> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QPolitica(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QPolitica(PathMetadata metadata, PathInits inits) {
        this(Politica.class, metadata, inits);
    }

    public QPolitica(Class<? extends Politica> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.area = inits.isInitialized("area") ? new QArea(forProperty("area"), inits.get("area")) : null;
        this.intranetFile = inits.isInitialized("intranetFile") ? new QIntranetFile(forProperty("intranetFile"), inits.get("intranetFile")) : null;
        this.normatividad = inits.isInitialized("normatividad") ? new QNormatividad(forProperty("normatividad")) : null;
        this.subarea = inits.isInitialized("subarea") ? new QSubarea(forProperty("subarea"), inits.get("subarea")) : null;
        this.usuario = inits.isInitialized("usuario") ? new QUsuario(forProperty("usuario"), inits.get("usuario")) : null;
    }

}

