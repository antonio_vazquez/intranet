package com.cie.intranet.services.logic;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrganizacionManagerServiceTest {

    @Autowired
    private OrganizacionManagerService organizacionManagerService;

    @Test
    public void test() throws ParseException {
        organizacionManagerService.syncLdif();
        assertThat(organizacionManagerService != null);
    }

}
