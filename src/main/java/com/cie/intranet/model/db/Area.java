package com.cie.intranet.model.db;


import javax.persistence.*;

@Entity
@Table(name = "area")
public class Area {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nombre;

    @OneToOne
    private Planta planta;

    @OneToOne
    private Normatividad normatividad;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Planta getPlanta() {
        return planta;
    }

    public void setPlanta(Planta planta) {
        this.planta = planta;
    }

    public Normatividad getNormatividad() {
        return normatividad;
    }

    public void setNormatividad(Normatividad normatividad) {
        this.normatividad = normatividad;
    }
}

