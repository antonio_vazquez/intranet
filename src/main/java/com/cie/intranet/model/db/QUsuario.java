package com.cie.intranet.model.db;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QUsuario is a Querydsl query type for Usuario
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QUsuario extends EntityPathBase<Usuario> {

    private static final long serialVersionUID = 38014198L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QUsuario usuario = new QUsuario("usuario");

    public final StringPath apellido = createString("apellido");

    public final StringPath contasena = createString("contasena");

    public final StringPath correo = createString("correo");

    public final StringPath descripcion = createString("descripcion");

    public final BooleanPath gerente = createBoolean("gerente");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath nombre = createString("nombre");

    public final StringPath nombreUsuario = createString("nombreUsuario");

    public final QRol rol;

    public final QUsuarioPerfil usuarioPerfil;

    public QUsuario(String variable) {
        this(Usuario.class, forVariable(variable), INITS);
    }

    public QUsuario(Path<? extends Usuario> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QUsuario(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QUsuario(PathMetadata metadata, PathInits inits) {
        this(Usuario.class, metadata, inits);
    }

    public QUsuario(Class<? extends Usuario> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.rol = inits.isInitialized("rol") ? new QRol(forProperty("rol")) : null;
        this.usuarioPerfil = inits.isInitialized("usuarioPerfil") ? new QUsuarioPerfil(forProperty("usuarioPerfil"), inits.get("usuarioPerfil")) : null;
    }

}

