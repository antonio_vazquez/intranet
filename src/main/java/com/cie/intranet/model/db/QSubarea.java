package com.cie.intranet.model.db;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QSubarea is a Querydsl query type for Subarea
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSubarea extends EntityPathBase<Subarea> {

    private static final long serialVersionUID = -1697281899L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QSubarea subarea = new QSubarea("subarea");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath nombre = createString("nombre");

    public final QNormatividad normatividad;

    public QSubarea(String variable) {
        this(Subarea.class, forVariable(variable), INITS);
    }

    public QSubarea(Path<? extends Subarea> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QSubarea(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QSubarea(PathMetadata metadata, PathInits inits) {
        this(Subarea.class, metadata, inits);
    }

    public QSubarea(Class<? extends Subarea> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.normatividad = inits.isInitialized("normatividad") ? new QNormatividad(forProperty("normatividad")) : null;
    }

}

