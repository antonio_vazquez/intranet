package com.cie.intranet.model.db;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QComunicado is a Querydsl query type for Comunicado
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QComunicado extends EntityPathBase<Comunicado> {

    private static final long serialVersionUID = 184578800L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QComunicado comunicado = new QComunicado("comunicado");

    public final StringPath contenido = createString("contenido");

    public final BooleanPath entregado = createBoolean("entregado");

    public final StringPath estatus = createString("estatus");

    public final StringPath extension = createString("extension");

    public final StringPath filename = createString("filename");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final QIntranetFile intranetFile;

    public final StringPath lista = createString("lista");

    public final StringPath nombre = createString("nombre");

    public final ArrayPath<byte[], Byte> pdf = createArray("pdf", byte[].class);

    public final QPlanta planta;

    public final DateTimePath<java.util.Date> register = createDateTime("register", java.util.Date.class);

    public final StringPath titulo = createString("titulo");

    public final StringPath username = createString("username");

    public final QUsuario usuario;

    public QComunicado(String variable) {
        this(Comunicado.class, forVariable(variable), INITS);
    }

    public QComunicado(Path<? extends Comunicado> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QComunicado(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QComunicado(PathMetadata metadata, PathInits inits) {
        this(Comunicado.class, metadata, inits);
    }

    public QComunicado(Class<? extends Comunicado> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.intranetFile = inits.isInitialized("intranetFile") ? new QIntranetFile(forProperty("intranetFile"), inits.get("intranetFile")) : null;
        this.planta = inits.isInitialized("planta") ? new QPlanta(forProperty("planta"), inits.get("planta")) : null;
        this.usuario = inits.isInitialized("usuario") ? new QUsuario(forProperty("usuario"), inits.get("usuario")) : null;
    }

}

