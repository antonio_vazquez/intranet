package com.cie.intranet.model.db;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QIntranetFile is a Querydsl query type for IntranetFile
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QIntranetFile extends EntityPathBase<IntranetFile> {

    private static final long serialVersionUID = -1650388013L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QIntranetFile intranetFile = new QIntranetFile("intranetFile");

    public final StringPath extension = createString("extension");

    public final StringPath filename = createString("filename");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath original = createString("original");

    public final ArrayPath<byte[], Byte> pdf = createArray("pdf", byte[].class);

    public final QPolitica politica;

    public final DateTimePath<java.util.Date> register = createDateTime("register", java.util.Date.class);

    public final StringPath tipo = createString("tipo");

    public QIntranetFile(String variable) {
        this(IntranetFile.class, forVariable(variable), INITS);
    }

    public QIntranetFile(Path<? extends IntranetFile> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QIntranetFile(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QIntranetFile(PathMetadata metadata, PathInits inits) {
        this(IntranetFile.class, metadata, inits);
    }

    public QIntranetFile(Class<? extends IntranetFile> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.politica = inits.isInitialized("politica") ? new QPolitica(forProperty("politica"), inits.get("politica")) : null;
    }

}

