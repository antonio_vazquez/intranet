package com.cie.intranet.model.db;


import org.apache.directory.api.ldap.model.message.controls.PagedResultsImpl;

import javax.persistence.*;

@Entity
@Table(name = "subarea")
public class Subarea {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nombre;

    @OneToOne
    private Area area;

    @OneToOne
    private Normatividad normatividad;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Normatividad getNormatividad() {
        return normatividad;
    }

    public void setNormatividad(Normatividad normatividad) {
        this.normatividad = normatividad;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }
}

