package com.cie.intranet.model.db;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QPlanta is a Querydsl query type for Planta
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPlanta extends EntityPathBase<Planta> {

    private static final long serialVersionUID = -10420082L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPlanta planta = new QPlanta("planta");

    public final StringPath descripcion = createString("descripcion");

    public final QDivision division;

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath nombre = createString("nombre");

    public QPlanta(String variable) {
        this(Planta.class, forVariable(variable), INITS);
    }

    public QPlanta(Path<? extends Planta> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QPlanta(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QPlanta(PathMetadata metadata, PathInits inits) {
        this(Planta.class, metadata, inits);
    }

    public QPlanta(Class<? extends Planta> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.division = inits.isInitialized("division") ? new QDivision(forProperty("division")) : null;
    }

}

