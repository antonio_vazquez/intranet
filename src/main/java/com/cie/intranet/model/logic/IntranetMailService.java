package com.cie.intranet.model.logic;

import com.cie.intranet.model.db.Comunicado;
import com.cie.intranet.model.db.Organizacion;
import com.cie.intranet.model.db.Usuario;
import com.cie.intranet.model.db.UsuarioPlanta;
import com.cie.intranet.services.db.ComunicadoService;
import com.cie.intranet.services.db.OrganizacionService;
import com.cie.intranet.services.db.UsuarioPlantaService;
import com.cie.intranet.services.db.UsuarioService;
import com.google.common.io.ByteSource;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;


@Component
@SuppressWarnings("Duplicates")
public class IntranetMailService {

    private static final Logger LOGGER = LoggerFactory.getLogger(IntranetMailService.class);

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private ComunicadoService comunicadoService;

    @Autowired
    private OrganizacionService organizacionService;

    @Autowired
    private UsuarioPlantaService usuarioPlantaService;

    @Autowired
    private UsuarioService usuarioService;

    public void sender(Long id) throws Exception {
        try{
            Comunicado comunicado = comunicadoService.findOne(id);
            List<Organizacion> organizacionList = organizacionService.findAllByPlant(comunicado.getPlanta().getNombre());
            String [] listaDistribucion = new String[organizacionList.size()];
            int index =0, indexGerente =0, indexGerenteOk =0;
            for (Organizacion organizacion : organizacionList){
                        listaDistribucion [index] =  organizacion.getCorreo();
                        index++;
            }

            List<UsuarioPlanta> usuarioPlantaList = usuarioPlantaService.findAllByPlantaId(comunicado.getPlanta().getId());
            for(UsuarioPlanta usuarioPlanta: usuarioPlantaList){
                if (usuarioService.findOne(usuarioPlanta.getUsuario().getId()).isGerente()){
                    indexGerente++;
                }
            }
            String [] listaDistribucionGerentes = new String[indexGerente];
            for(UsuarioPlanta usuarioPlanta: usuarioPlantaList){
                if (usuarioService.findOne(usuarioPlanta.getUsuario().getId()).isGerente()){
                    listaDistribucionGerentes [indexGerenteOk] = (usuarioService.findOne(usuarioPlanta.getUsuario().getId()).getCorreo())+"@cieautomotive.com";
                    indexGerenteOk++;
                }
            }
/*
            for (String s : listaDistribucion){
                LOGGER.error("Lista -> "+ s);
            }
            ArrayList<String> ar = new ArrayList<String>();
            for (Organizacion organizacion : organizacionList){
               ar.add(organizacion.getCorreo());
            }
*/

            byte[] report = comunicado.getPdf();

            // ***************************CODIGO PRUEBA
            InputStream targetStream = new ByteArrayInputStream(report);
            InputStream inputStream = IOUtils.toInputStream(new String(report), StandardCharsets.UTF_8);
            InputStream targetStream1 = ByteSource.wrap(report).openStream();
            ByteArrayResource resource = new ByteArrayResource(IOUtils.toByteArray(inputStream));
            InputStream myInputStream = new ByteArrayInputStream(report);
            ByteArrayInputStream bis = new ByteArrayInputStream(report);
            ByteArrayDataSource bds = new ByteArrayDataSource(report, "AttName");
            // ***************************CODIGO PRUEBA

            XSSFSheet sheet;
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
            mimeMessageHelper.setFrom("internal.message@cieautomotive.com");
            mimeMessageHelper.setSubject(comunicado.getTitulo());
            /*        mimeMessageHelper.setTo("internal.message@cieautomotive.com");*/
            mimeMessageHelper.setTo(listaDistribucion);

/*            mimeMessageHelper.setCc("isanchez@cieautomotive.com");*/
            mimeMessageHelper.setCc(listaDistribucionGerentes);
            mimeMessageHelper.addAttachment(comunicado.getFilename(), new ByteArrayResource(report));


            //mimeMessageHelper.setCc(new String[]{"antonio.vazquez@mapremex.com.mx"});
            /*        mimeMessageHelper.setCc(new String[]{"lfrias@cieautomotive.com"});*/
            //mimeMessageHelper.setCc(new String[]{"rfbarron@cieautomotive.com","adrodriguez@cieautomotive.com","abacmeister@cieautomotive.com"});
            mimeMessageHelper.setText(msg(comunicado), true);
            javaMailSender.send(mimeMessageHelper.getMimeMessage());
        } catch (Exception e){
            LOGGER.error("Ha ocurrido un error en el envio de correo de la lista de dist:[ "+ExceptionUtils.getFullStackTrace(e)+ "]");
        }

    }



    public String msg(Comunicado comunicado){

        return "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "<title></title>\n" +
                "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" +
                "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n" +
                "<style type=\"text/css\">\n" +
                "    /* FONTS */\n" +
                "    @import url('https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i');\n" +
                "\n" +
                "    /* CLIENT-SPECIFIC STYLES */\n" +
                "    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }\n" +
                "    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }\n" +
                "    img { -ms-interpolation-mode: bicubic; }\n" +
                "\n" +
                "    /* RESET STYLES */\n" +
                "    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }\n" +
                "    table { border-collapse: collapse !important; }\n" +
                "    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }\n" +
                "\n" +
                "    /* iOS BLUE LINKS */\n" +
                "    a[x-apple-data-detectors] {\n" +
                "        color: inherit !important;\n" +
                "        text-decoration: none !important;\n" +
                "        font-size: inherit !important;\n" +
                "        font-family: inherit !important;\n" +
                "        font-weight: inherit !important;\n" +
                "        line-height: inherit !important;\n" +
                "    }\n" +
                "\n" +
                "    /* MOBILE STYLES */\n" +
                "    @media screen and (max-width:600px){\n" +
                "        h1 {\n" +
                "            font-size: 32px !important;\n" +
                "            line-height: 32px !important;\n" +
                "        }\n" +
                "    }\n" +
                "\n" +
                "    /* ANDROID CENTER FIX */\n" +
                "    div[style*=\"margin: 16px 0;\"] { margin: 0 !important; }\n" +
                "</style>\n" +
                "</head>\n" +
                "<body style=\"background-color: #f3f5f7; margin: 0 !important; padding: 0 !important;\">\n" +
                "\n" +
                "<!-- HIDDEN PREHEADER TEXT -->\n" +
                "<div style=\"display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Poppins', sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;\">\n" +
                "    &iexcl;Tienes un nuevo comunicado&#33;. Porfavor abre tu correo para obtener m&aacute;s detalles.\n" +
                "</div>\n" +
                "\n" +
                "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n" +
                "    <!-- LOGO -->\n" +
                "    <tr>\n" +
                "        <td align=\"center\">\n" +
                "            <!--[if (gte mso 9)|(IE)]>\n" +
                "            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"600\">\n" +
                "            <tr>\n" +
                "            <td align=\"center\" valign=\"top\" width=\"600\">\n" +
                "            <![endif]-->\n" +
                "            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n" +
                "                <tr>\n" +
                "                    <td align=\"center\" valign=\"top\" style=\"padding: 40px 10px 10px 10px;\">\n" +
                "                        <a href=\"#\" target=\"_blank\" style=\"text-decoration: none;\">\n" +
                "\t\t\t\t\t\t\t<span style=\"display: block; font-family: 'Poppins', sans-serif; color: #3e8ef7; font-size: 36px;\" border=\"0\"><b>CIE</b> Automotive</span>\n" +
                "                        </a>\n" +
                "                    </td>\n" +
                "                </tr>\n" +
                "            </table>\n" +
                "            <!--[if (gte mso 9)|(IE)]>\n" +
                "            </td>\n" +
                "            </tr>\n" +
                "            </table>\n" +
                "            <![endif]-->\n" +
                "        </td>\n" +
                "    </tr>\n" +
                "    <!-- HERO -->\n" +
                "    <tr>\n" +
                "        <td align=\"center\" style=\"padding: 0px 10px 0px 10px;\">\n" +
                "            <!--[if (gte mso 9)|(IE)]>\n" +
                "            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"600\">\n" +
                "            <tr>\n" +
                "            <td align=\"center\" valign=\"top\" width=\"600\">\n" +
                "            <![endif]-->\n" +
                "            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n" +
                "                <tr>\n" +
                "                    <td bgcolor=\"#ffffff\" align=\"center\" valign=\"top\" style=\"padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px;\">\n" +
                "                      <h1 style=\"font-size: 36px; font-weight: 600; margin: 0; font-family: 'Poppins', sans-serif;\">Intranet corporativa</h1>\n" +
                "                    </td>\n" +
                "                </tr>\n" +
                "            </table>\n" +
                "            <!--[if (gte mso 9)|(IE)]>\n" +
                "            </td>\n" +
                "            </tr>\n" +
                "            </table>\n" +
                "            <![endif]-->\n" +
                "        </td>\n" +
                "    </tr>\n" +
                "    <!-- COPY BLOCK -->\n" +
                "    <tr>\n" +
                "        <td align=\"center\" style=\"padding: 0px 10px 0px 10px;\">\n" +
                "            <!--[if (gte mso 9)|(IE)]>\n" +
                "            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"600\">\n" +
                "            <tr>\n" +
                "            <td align=\"center\" valign=\"top\" width=\"600\">\n" +
                "            <![endif]-->\n" +
                "            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n" +
                "              <!-- COPY -->\n" +
                "              <tr>\n" +
                "                <td bgcolor=\"#ffffff\" align=\"left\" style=\"padding: 20px 30px 20px 30px; color: #666666; font-family: 'Poppins', sans-serif; font-size: 16px; font-weight: 400; line-height: 25px;\">\n" +
                "                  <p style=\"margin: 0;\">Estimado usuario, se ha generado un nuevo comunicado.</p>\n" +
                "                  <p style=\"margin: 0;\">Planta: "+comunicado.getPlanta().getNombre()+" </p>\n" +
                "                  <p style=\"margin: 0;\">Titulo: "+comunicado.getTitulo()+"</p>\n" +
                "                  <p style=\"margin: 0;\">Fecha: "+comunicado.getRegister()+"</p>\n" +
                "                  <p style=\"margin: 0;\">Contenido: "+comunicado.getContenido()+"</p>\n" +
                "                </td>\n" +
                "              </tr>\n" +
                "              <!-- BULLETPROOF BUTTON -->\n" +
                "              <tr>\n" +
                "                <td bgcolor=\"#ffffff\" align=\"center\">\n" +
                "                  <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                "                    <tr>\n" +
                "                      <td bgcolor=\"#ffffff\" align=\"center\" style=\"padding: 20px 30px 30px 30px;\">\n" +
                "                        <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                "                          <tr>\n" +
                "                          </tr>\n" +
                "                        </table>\n" +
                "                      </td>\n" +
                "                    </tr>\n" +
                "                  </table>\n" +
                "                </td>\n" +
                "              </tr>\n" +
                "              <!-- COPY -->\n" +
                "              <tr>\n" +
                "              </tr>\n" +
                "              <!-- COPY -->\n" +
                "              <tr>\n" +
                "                <td bgcolor=\"#ffffff\" align=\"left\" style=\"padding: 0px 30px 40px 30px; border-radius: 0px 0px 0px 0px; color: #666666; font-family: 'Poppins', sans-serif; font-size: 14px; font-weight: 400; line-height: 25px;\">\n" +
                "                  <p style=\"margin: 0;\">IT,<br>Divisi&oacute;n Mecanizado NAFTA</p>\n" +
                "                </td>\n" +
                "              </tr>\n" +
                "            </table>\n" +
                "            <!--[if (gte mso 9)|(IE)]>\n" +
                "            </td>\n" +
                "            </tr>\n" +
                "            </table>\n" +
                "            <![endif]-->\n" +
                "        </td>\n" +
                "    </tr>\n" +
                "    <!-- FOOTER -->\n" +
                "    <tr>\n" +
                "        <td align=\"center\" style=\"padding: 10px 10px 50px 10px;\">\n" +
                "            <!--[if (gte mso 9)|(IE)]>\n" +
                "            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"600\">\n" +
                "            <tr>\n" +
                "            <td align=\"center\" valign=\"top\" width=\"600\">\n" +
                "            <![endif]-->\n" +
                "            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n" +
                "              <!-- NAVIGATION -->\n" +
                "              <tr>\n" +
                "                <td bgcolor=\"#ffffff\" align=\"left\" style=\"padding: 30px 30px 30px 30px; color: #aaaaaa; font-family: 'Poppins', sans-serif; font-size: 12px; font-weight: 400; line-height: 18px;\">\n" +
                "                  <p style=\"margin: 0;\">\n" +
                "                  </p>\n" +
                "                </td>\n" +
                "              </tr>\n" +
                "              <!-- PERMISSION REMINDER -->\n" +
                "              <tr>\n" +
                "                <td bgcolor=\"#ffffff\" align=\"left\" style=\"padding: 0px 30px 30px 30px; color: #aaaaaa; font-family: 'Poppins', sans-serif; font-size: 12px; font-weight: 400; line-height: 18px;\">\n" +
                "                  <p style=\"margin: 0;\">Mensaje enviado autom&aacute;ticamente<a href=\"#\" target=\"_blank\" style=\"color: #999999; font-weight: 700;\"></a>.</p>\n" +
                "                </td>\n" +
                "              </tr>\n" +
                "              <!-- UNSUBSCRIBE -->\n" +
                "              <tr>\n" +
                "                <td bgcolor=\"#ffffff\" align=\"left\" style=\"padding: 0px 30px 30px 30px; color: #aaaaaa; font-family: 'Poppins', sans-serif; font-size: 14px; font-weight: 400; line-height: 18px;\">\n" +
                "                  <p style=\"margin: 0;\">Si tienes problemas con visualizar el aplicativo web por favor env&iacute;a correo a :<a href=\"mailto:soportetinafta@cieautomotive.com?Subject=Error%20en%20aplicaci&oacute;n%20de%20Intranet%20Corporativa&body=Hola%20buen%20d&iacute;a%20necesito%20de%20su%20soporte%20el%20error%20es:\" target=\"_blank\" style=\"color: #999999; font-weight: 700;\">Soporte TI NAFTA</a>.</p>\n" +
                "                </td>\n" +
                "              </tr>\n" +
                "              <!-- ADDRESS -->\n" +
                "              <tr>\n" +
                "                <td bgcolor=\"#ffffff\" align=\"left\" style=\"padding: 0px 30px 30px 30px; color: #aaaaaa; font-family: 'Poppins', sans-serif; font-size: 12px; font-weight: 400; line-height: 18px;\">\n" +
                "                </td>\n" +
                "              </tr>\n" +
                "\t\t      <!-- COPYRIGHT -->\n" +
                "              <tr>\n" +
                "                <td align=\"center\" style=\"padding: 30px 30px 30px 30px; color: #333333; font-family: 'Poppins', sans-serif; font-size: 12px; font-weight: 400; line-height: 18px;\">\n" +
                "                  <p style=\"margin: 0;\">Copyright © 2022 CIE Automotive. All rights reserved.</p>\n" +
                "                </td>\n" +
                "              </tr>\n" +
                "            </table>\n" +
                "            <!--[if (gte mso 9)|(IE)]>\n" +
                "            </td>\n" +
                "            </tr>\n" +
                "            </table>\n" +
                "            <![endif]-->\n" +
                "        </td>\n" +
                "    </tr>\n" +
                "</table>\n" +
                "\n" +
                "</body>\n" +
                "</html>\n";
    }



}
