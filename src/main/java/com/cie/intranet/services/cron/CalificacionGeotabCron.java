package com.cie.intranet.services.cron;

import com.cie.intranet.services.logic.OrganizacionManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class CalificacionGeotabCron {

    @Autowired
    private OrganizacionManagerService organizacionManagerService;

    @Scheduled(cron = "0 0/60 * * * ?")
    public void geoConsoleLocation() {
        //organizacionManagerService.syncLdif();f|
        organizacionManagerService.syncDirContext();
    }


}
