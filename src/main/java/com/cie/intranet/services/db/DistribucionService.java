package com.cie.intranet.services.db;

import com.cie.intranet.model.db.Distribucion;
import com.cie.intranet.repository.DistribucionRepository;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

@Service
@Transactional
public class DistribucionService extends AbstractService<Distribucion, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(DistribucionService.class);

    @Autowired
    private DistribucionRepository repository;

    @PostConstruct
    public void manage() {
        setRepository(repository);
        setLOGGER(LOGGER);
    }

    public Distribucion findByComunicado(Long comunicadoId) {
        try {
            return repository.findByComunicadoId(comunicadoId);
        } catch (Exception e) {
            LOGGER.error("Error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            return null;
        }
    }

    public Distribucion syncDistribucion(Distribucion distribucion) {
        try {
            Distribucion prev = repository.findByComunicadoId(distribucion.getComunicado().getId());
            if (prev != null && prev.getId() > 0) {
                this.delete(prev);
            }
            return this.insert(distribucion);
        } catch (Exception e) {
            LOGGER.error("Error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            return null;
        }
    }

}
