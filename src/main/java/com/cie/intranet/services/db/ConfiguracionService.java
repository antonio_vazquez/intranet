package com.cie.intranet.services.db;

import com.cie.intranet.model.db.Configuracion;
import com.cie.intranet.repository.ConfiguracionRepository;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Transactional
@SuppressWarnings("Duplicates")
public class ConfiguracionService extends AbstractService<Configuracion, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConfiguracionService.class);

    @Autowired
    private ConfiguracionRepository repository;

    @PostConstruct
    public void manage() {
        setRepository(repository);
        setLOGGER(LOGGER);
    }

    public Configuracion findParametro(String parametro) {
        try {
            return repository.findByParametro(parametro);
        } catch (Exception e) {
            LOGGER.error("Error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            return null;
        }
    }

    public Map<String, Configuracion> buildMap() {
        try {
            return repository.findAll().stream().collect(Collectors.toMap(p->p.getParametro(), p->p));
        } catch (Exception e) {
            LOGGER.error("Error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            return null;
        }
    }

}
