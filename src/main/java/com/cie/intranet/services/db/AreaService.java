package com.cie.intranet.services.db;

import com.cie.intranet.model.db.Area;
import com.cie.intranet.repository.AreaRepository;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

@Service
@Transactional
public class AreaService extends AbstractService<Area, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AreaService.class);

    @Autowired
    private AreaRepository repository;

    @Autowired
    private AreaService areaService;

    @PostConstruct
    public void manage() {
        setRepository(repository);
        setLOGGER(LOGGER);
    }

    @Override
    public Area update(Area r) {
        Area toUpdate = new Area();
        try {
            toUpdate =  areaService.findOne(r.getId());
            toUpdate.setNombre(r.getNombre());

        } catch (Exception e) {
            LOGGER.error("Ocurrio un error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
        }
        return toUpdate;
    }


}
