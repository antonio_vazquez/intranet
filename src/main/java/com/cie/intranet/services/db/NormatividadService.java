package com.cie.intranet.services.db;

import com.cie.intranet.model.db.Area;
import com.cie.intranet.model.db.Normatividad;
import com.cie.intranet.repository.AreaRepository;
import com.cie.intranet.repository.NormatividadRepository;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

@Service
@Transactional
public class NormatividadService extends AbstractService<Normatividad, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(NormatividadService.class);

    @Autowired
    private NormatividadRepository repository;

    @Autowired
    private NormatividadService normatividadService;

    @PostConstruct
    public void manage() {
        setRepository(repository);
        setLOGGER(LOGGER);
    }

    @Override
    public Normatividad update(Normatividad r) {
        Normatividad toUpdate = new Normatividad();
        try {
            toUpdate =  normatividadService.findOne(r.getId());
            toUpdate.setNombre(r.getNombre());

        } catch (Exception e) {
            LOGGER.error("Ocurrio un error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
        }
        return toUpdate;
    }


}
