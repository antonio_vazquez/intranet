package com.cie.intranet.services.db;

import com.cie.intranet.model.db.Procedimiento;
import com.cie.intranet.repository.ProcedimientoRepository;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

@Service
@Transactional
public class ProcedimientoService extends AbstractService<Procedimiento, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProcedimientoService.class);

    @Autowired
    private ProcedimientoRepository repository;

    @Autowired
    private ProcedimientoService procedimientoService;

    @PostConstruct
    public void manage() {
        setRepository(repository);
        setLOGGER(LOGGER);
    }

    @Override
    public Procedimiento update(Procedimiento r) {
        Procedimiento toUpdate = new Procedimiento();
        try {
            toUpdate =  procedimientoService.findOne(r.getId());
            toUpdate.setNombre(r.getNombre());
            toUpdate.setCodigo(r.getCodigo());
            toUpdate.setDescripcion(r.getDescripcion());
            if (!r.getFile().isEmpty()){
                toUpdate.setPdf(r.getFile().getBytes());
                toUpdate.setExtension(r.getExtension());
                toUpdate.setFilename(r.getFilename());
            }

        } catch (Exception e) {
            LOGGER.error("Ocurrio un error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
        }
        return toUpdate;
    }

}
