package com.cie.intranet.services.db;

import com.cie.intranet.model.db.Usuario;
import com.cie.intranet.model.db.UsuarioPerfil;
import com.cie.intranet.repository.UsuarioPerfilRepository;
import com.cie.intranet.repository.UsuarioRepository;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

@Service
@Transactional
public class UsuarioPerfilService extends AbstractService<UsuarioPerfil, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(UsuarioPerfilService.class);

    @Autowired
    private UsuarioPerfilRepository repository;

    @Autowired
    private UsuarioPerfilService usuarioPerfilService;

    @PostConstruct
    public void manage() {
        setRepository(repository);
        setLOGGER(LOGGER);
    }

    @Override
    public UsuarioPerfil update(UsuarioPerfil r) {
        UsuarioPerfil toUpdate = new UsuarioPerfil();
        try {
            toUpdate =  usuarioPerfilService.findOne(r.getId());
            toUpdate.setUsuario(r.getUsuario());
            toUpdate.setRol(r.getRol());

        } catch (Exception e) {
            LOGGER.error("Ocurrio un error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
        }
        return toUpdate;
    }

    public UsuarioPerfil findByUsuarioId(Long id){
        UsuarioPerfil usuarioPerfil = repository.findByUsuarioId(id);

        return usuarioPerfil;
    }

    public UsuarioPerfil findByUsuarioIdAndRolId(Long idUsuario, Long idRol){
        UsuarioPerfil usuarioPerfil = repository.findByUsuario_IdAndAndRol_Id(idUsuario, idRol);

        return usuarioPerfil;
    }

}
