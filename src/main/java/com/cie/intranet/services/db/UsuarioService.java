package com.cie.intranet.services.db;

import com.cie.intranet.model.db.Usuario;
import com.cie.intranet.repository.UsuarioRepository;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

@Service
@Transactional
public class UsuarioService extends AbstractService<Usuario, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(UsuarioService.class);

    @Autowired
    private UsuarioRepository repository;

    @Autowired
    private UsuarioService usuarioService;

    @PostConstruct
    public void manage() {
        setRepository(repository);
        setLOGGER(LOGGER);
    }

    @Override
    public Usuario update(Usuario r) {
        Usuario toUpdate = new Usuario();
        try {
            toUpdate =  usuarioService.findOne(r.getId());
            toUpdate.setNombre(r.getNombre());
            toUpdate.setApellido(r.getApellido());
            toUpdate.setContasena(r.getContasena());
            toUpdate.setDescripcion(r.getDescripcion());
            toUpdate.setNombreUsuario(r.getNombreUsuario());
            toUpdate.setCorreo(r.getCorreo());
            toUpdate.setRol(r.getRol());
            toUpdate.setGerente(r.isGerente());

        } catch (Exception e) {
            LOGGER.error("Ocurrio un error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
        }
        return toUpdate;
    }

    public Usuario findByCorreo(String correo) {
        try {
            return repository.findByCorreo(correo);
        } catch (Exception e) {
            LOGGER.error("Ocurrio un error durante la eliminacion:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            return new Usuario();
        }
    }

}
