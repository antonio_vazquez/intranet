package com.cie.intranet.services.db;

import com.cie.intranet.form.ComunicadoForm;
import com.cie.intranet.model.db.*;
import com.cie.intranet.repository.ComunicadoRepository;
import com.cie.intranet.repository.PoliticaRepository;
import com.querydsl.core.BooleanBuilder;
import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.swing.*;
import javax.transaction.Transactional;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class ComunicadoService extends AbstractService<Comunicado, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ComunicadoService.class);

    @Autowired
    private ComunicadoRepository repository;

    @Autowired
    private ComunicadoService comunicadoService;

    @PostConstruct
    public void manage() {
        setRepository(repository);
        setLOGGER(LOGGER);
    }

    @Override
    public Comunicado update(Comunicado r) {
        Comunicado toUpdate = new Comunicado();
        try {
            toUpdate =  comunicadoService.findOne(r.getId());
            toUpdate.setNombre(r.getNombre());
            toUpdate.setTitulo(r.getTitulo());
            toUpdate.setContenido(r.getContenido());
            if (!r.getFile().isEmpty()){
                toUpdate.setPdf(r.getFile().getBytes());
            }

        } catch (Exception e) {
            LOGGER.error("Ocurrio un error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
        }
        return toUpdate;
    }

    public Comunicado updateEntrega(Comunicado r){
        Comunicado toUpdate = new Comunicado();
        try {
            toUpdate =  comunicadoService.findOne(r.getId());
            toUpdate.setEntregado(r.isEntregado());

        } catch (Exception e) {
            LOGGER.error("Ocurrio un error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
        }
        return toUpdate;
    }

    public List<Comunicado> findAllByPlanta(List<Planta>  plantaList){
        List<Comunicado> comunicadoList = repository.findAllByPlantaIn(plantaList);
        return comunicadoList;
    }

    public List<Comunicado> findAllByPlantaAndId(List<Planta>  plantaList, Long id){
        List<Comunicado> comunicadoList = repository.findAllByPlantaInAndId(plantaList, id);
        return comunicadoList;
    }

    public List<Comunicado> queryBuilder(ComunicadoForm comunicadoForm) {
        try {
            QComunicado comunicado = QComunicado.comunicado;
            com.querydsl.core.BooleanBuilder where = new com.querydsl.core.BooleanBuilder();
            if (comunicadoForm.getDesde() != null) {
                where.and(comunicado.register.after(comunicadoForm.getDesde()));
            }
            if (comunicadoForm.getHasta() != null) {
                where.and(comunicado.register.before(comunicadoForm.getHasta()));
            }
            if (!StringUtils.isEmpty(comunicadoForm.getNombre())) {
                where.and(comunicado.titulo.contains(comunicadoForm.getNombre()));
            }

            if (comunicadoForm.getPlanta() != null && comunicadoForm.getPlanta() > 0){
                where.and(comunicado.planta.id.eq(comunicadoForm.getPlanta()));
            }
            return IteratorUtils.toList(this.repository.findAll(where.getValue()).iterator());
        } catch (Exception e) {
            LOGGER.error("Ocurrio un error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            return null;
        }
    }

    public String stripAccents(String s)
    {
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        return s;
    }

}
