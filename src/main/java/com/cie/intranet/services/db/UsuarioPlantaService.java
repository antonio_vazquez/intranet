package com.cie.intranet.services.db;

import com.cie.intranet.model.db.UsuarioPerfil;
import com.cie.intranet.model.db.UsuarioPlanta;
import com.cie.intranet.repository.UsuarioPerfilRepository;
import com.cie.intranet.repository.UsuarioPlantaRepository;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class UsuarioPlantaService extends AbstractService<UsuarioPlanta, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(UsuarioPlantaService.class);

    @Autowired
    private UsuarioPlantaRepository repository;

    @Autowired
    private UsuarioPlantaService usuarioPlantaService;

    @PostConstruct
    public void manage() {
        setRepository(repository);
        setLOGGER(LOGGER);
    }

    @Override
    public UsuarioPlanta update(UsuarioPlanta r) {
        UsuarioPlanta toUpdate = new UsuarioPlanta();
        try {
            toUpdate =  usuarioPlantaService.findOne(r.getId());
            toUpdate.setUsuario(r.getUsuario());
            toUpdate.setPlanta(r.getPlanta());

        } catch (Exception e) {
            LOGGER.error("Ocurrio un error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
        }
        return toUpdate;
    }

    public List<UsuarioPlanta> findByUsuario(Long idUsuario){
        List<UsuarioPlanta> usuarioPlantaList = repository.findAllByUsuarioId(idUsuario);

        return  usuarioPlantaList;
    }

    public UsuarioPlanta findByUsuarioIdAndPlantaId (Long idUsuario, Long idPlanta){
        UsuarioPlanta usuarioPlanta = repository.findByUsuario_IdAndPlanta_Id(idUsuario, idPlanta);

        return usuarioPlanta;
    }

    public List<UsuarioPlanta> findAllByPlantaId (Long idPlanta){
        List<UsuarioPlanta> usuarioPlantaList = repository.findAllByPlantaId(idPlanta);

        return usuarioPlantaList;
    }

}
