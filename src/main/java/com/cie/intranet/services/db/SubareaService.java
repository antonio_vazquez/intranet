package com.cie.intranet.services.db;

import com.cie.intranet.model.db.Area;
import com.cie.intranet.model.db.Subarea;
import com.cie.intranet.repository.AreaRepository;
import com.cie.intranet.repository.SubareaRepository;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

@Service
@Transactional
public class SubareaService extends AbstractService<Subarea, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(SubareaService.class);

    @Autowired
    private SubareaRepository repository;

    @Autowired
    private SubareaService subareaService;

    @PostConstruct
    public void manage() {
        setRepository(repository);
        setLOGGER(LOGGER);
    }

    @Override
    public Subarea update(Subarea r) {
        Subarea toUpdate = new Subarea();
        try {
            toUpdate =  subareaService.findOne(r.getId());
            toUpdate.setNombre(r.getNombre());

        } catch (Exception e) {
            LOGGER.error("Ocurrio un error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
        }
        return toUpdate;
    }


}
