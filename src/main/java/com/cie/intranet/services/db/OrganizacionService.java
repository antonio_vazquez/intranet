package com.cie.intranet.services.db;

import com.cie.intranet.model.db.Configuracion;
import com.cie.intranet.model.db.Organizacion;
import com.cie.intranet.repository.ConfiguracionRepository;
import com.cie.intranet.repository.OrganizacionRepository;
import jdk.internal.dynalink.linker.LinkerServices;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.aspectj.weaver.ast.Or;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Transactional
@SuppressWarnings("Duplicates")
public class OrganizacionService extends AbstractService<Organizacion, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrganizacionService.class);

    @Autowired
    private OrganizacionRepository repository;

    @PostConstruct
    public void manage() {
        setRepository(repository);
        setLOGGER(LOGGER);
    }

    public List<Organizacion> findAllByPlant(String planta){

        List<Organizacion> organizacionList = repository.findAllByCompaniaContainsAndCompaniaIsNotNullAndCorreoIsNotNull(planta);

        return organizacionList;
    }

}
