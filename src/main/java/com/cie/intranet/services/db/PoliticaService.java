package com.cie.intranet.services.db;

import com.cie.intranet.model.db.Politica;
import com.cie.intranet.model.db.Usuario;
import com.cie.intranet.repository.PoliticaRepository;
import com.cie.intranet.repository.UsuarioRepository;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class PoliticaService extends AbstractService<Politica, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(PoliticaService.class);

    @Autowired
    private PoliticaRepository repository;

    @Autowired
    private PoliticaService politicaService;

    @PostConstruct
    public void manage() {
        setRepository(repository);
        setLOGGER(LOGGER);
    }

    @Override
    public Politica update(Politica r) {
        Politica toUpdate = new Politica();
        try {
            toUpdate =  politicaService.findOne(r.getId());
            toUpdate.setNombre(r.getNombre());
            toUpdate.setCodigo(r.getCodigo());
            toUpdate.setDescripcion(r.getDescripcion());
            toUpdate.setArea(r.getArea());
            if (!r.getFile().isEmpty()){
                toUpdate.setPdf(r.getFile().getBytes());
                toUpdate.setExtension(r.getExtension());
                toUpdate.setFilename(r.getFilename());
            }

        } catch (Exception e) {
            LOGGER.error("Ocurrio un error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
        }
        return toUpdate;
    }

    public List<Politica> findAllByNormatividadId (Long id){
        List<Politica> politicaList = new ArrayList<>();
        try {
            return repository.findAllByNormatividadId(id);
        }catch (Exception e){
            LOGGER.error("Ocurrio un error:[" + ExceptionUtils.getFullStackTrace(e) +"]");
            return null;
        }
    }

}
