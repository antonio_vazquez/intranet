package com.cie.intranet.services.db;

import com.cie.intranet.model.db.Planta;
import com.cie.intranet.model.db.UsuarioPlanta;
import com.cie.intranet.repository.PlantaRepository;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class PlantaService extends AbstractService<Planta, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(PlantaService.class);

    @Autowired
    private PlantaRepository repository;

    @Autowired
    private PlantaService plantaService;

    @PostConstruct
    public void manage() {
        setRepository(repository);
        setLOGGER(LOGGER);
    }

    @Override
    public Planta update(Planta r) {
        Planta toUpdate = new Planta();
        try {
            toUpdate =  plantaService.findOne(r.getId());
            toUpdate.setNombre(r.getNombre());
            toUpdate.setDescripcion(r.getDescripcion());

        } catch (Exception e) {
            LOGGER.error("Ocurrio un error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
        }
        return toUpdate;
    }

}
