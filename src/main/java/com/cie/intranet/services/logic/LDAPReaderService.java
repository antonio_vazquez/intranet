package com.cie.intranet.services.logic;

import com.cie.intranet.model.db.Organizacion;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.directory.*;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

@Service
public class LDAPReaderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LDAPReaderService.class);

    @Value("${app.ldap.context.factory}")
    private String ldapContextFactory;

    @Value("${app.ldap.url}")
    private String ldapUrl;

    @Value("${app.ldap.security.authenticacion}")
    private String ldapSecurityAuthentication;

    @Value("${app.ldap.security.principal}")
    private String ldapSecurityPrincipal;

    @Value("${app.ldap.criteria.filter}")
    private String ldapCriteriaFilter;

    @Value("${app.ldap.security.user}")
    private String ldapUser;

    @Value("${app.ldap.security.password}")
    private String ldapPassword;

    public List<Organizacion> buildEntry() {
        int i = 0;
        List<Organizacion> entryList = new ArrayList<>();
        try {
            DirContext ctx = null;
            Hashtable env = new Hashtable(11);
            env.put(Context.INITIAL_CONTEXT_FACTORY, ldapContextFactory);
            env.put(Context.PROVIDER_URL, ldapUrl);
            env.put(Context.SECURITY_AUTHENTICATION, ldapSecurityAuthentication);
            env.put(Context.SECURITY_PRINCIPAL, ldapSecurityPrincipal);
            env.put(Context.SECURITY_CREDENTIALS, ldapPassword);
            env.put("com.sun.jndi.ldap.connect.timeout", "30000");
            env.put("java.naming.ldap.referral.limit", "5");
            env.put(Context.REFERRAL, "ignore");
            ctx = new InitialDirContext(env);
            ctx.addToEnvironment("java.naming.referral","follow");
            SearchControls ctl = new SearchControls();
            ctl.setSearchScope(SearchControls.SUBTREE_SCOPE);
            // Attributos de busqueda
            //ctl.setReturningAttributes(new String [] {"company", "mail", "department", "name", "title", "distinguishedName"});
            NamingEnumeration answer = ctx.search("", ldapCriteriaFilter, ctl);
            while (answer.hasMore()) {
                try {
                    SearchResult searchResult = (SearchResult) answer.next();
                    Attributes attributes = searchResult.getAttributes();
                    NamingEnumeration<? extends Attribute> attrs = attributes.getAll();
                    Organizacion lde = new Organizacion();
                    try {
                        while (attrs.hasMore()) {
                            Attribute attribute = attrs.next();
                            if (attribute.getID().equals("company")) {
                                   lde.setCompania(attribute.get().toString());
                            }
                            //if (attribute.getID().equals("mail")) {
                            if (attribute.getID().equals("mail")) {
                                lde.setCorreo(attribute.get().toString());
                            }
                            if (attribute.getID().equals("department")) {
                                lde.setDepartamento(attribute.get().toString());
                            }
                            if (attribute.getID().equals("name")) {
                                lde.setDescripcion(attribute.get().toString());
                            }
                            if (attribute.getID().equals("title")) {
                                lde.setTitulo(attribute.get().toString());
                            }
                            if (attribute.getID().equals("distinguishedName")) {
                                lde.setDn(attribute.get().toString());
                            }
                        }
                    } catch (Exception e) {
                        LOGGER.error("Error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
                    }
                    entryList.add(lde);
                    i++;
                } catch (Exception e) {
                    LOGGER.error("Error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
                }
            }
            ctx.close();
            return entryList;
        } catch (Exception e) {
            LOGGER.error("Error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            return entryList;
        }
    }

}
