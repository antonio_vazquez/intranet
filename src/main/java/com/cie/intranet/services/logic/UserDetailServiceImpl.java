package com.cie.intranet.services.logic;

import com.cie.intranet.model.db.Usuario;
import com.cie.intranet.model.db.UsuarioPerfil;
import com.cie.intranet.model.logic.AppUsuario;
import com.cie.intranet.services.db.UsuarioPerfilService;
import com.cie.intranet.services.db.UsuarioService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("userDetailService")
public class UserDetailServiceImpl implements UserDetailsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserDetailServiceImpl.class);

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private UsuarioPerfilService usuarioPerfilService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Usuario usuario = usuarioService.findByCorreo(username);
        UsuarioPerfil usuarioPerfil = usuarioPerfilService.findByUsuarioId(usuario.getId());
        List<GrantedAuthority> setAuths = null;
        if (usuario != null && usuario.getId() > 0) {
            //usuarioService.update(usuario);
            setAuths = new ArrayList<GrantedAuthority>();

            setAuths.add(new SimpleGrantedAuthority(usuarioPerfil.getRol().getNombre()));
        }
        return new AppUsuario(
                usuario.getCorreo(),
                usuario.getContasena(),
                true,
                true,
                true,
                true,
                setAuths,
                String.format("%s %s %s", usuario.getNombre(), usuario.getNombre(), usuario.getNombre()),
                null, null);
    }

}
