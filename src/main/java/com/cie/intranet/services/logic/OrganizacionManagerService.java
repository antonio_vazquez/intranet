package com.cie.intranet.services.logic;

import com.cie.intranet.model.db.Organizacion;
import com.cie.intranet.services.db.ConfiguracionService;
import com.cie.intranet.services.db.OrganizacionService;
import com.cie.intranet.util.Constant;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.directory.api.ldap.model.ldif.LdifEntry;
import org.apache.directory.api.ldap.model.ldif.LdifReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrganizacionManagerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrganizacionManagerService.class);

    @Autowired
    private ResourceLoader resourceLoader;

    @Autowired
    private OrganizacionService organizacionService;

    @Autowired
    private ConfiguracionService configuracionService;

    @Autowired
    private LDAPReaderService ldapReaderService;

    public void syncLdif() {
        try {
            String[] ou = configuracionService.findParametro(Constant.OU_LOOKUP).getValor().split(",");
            Resource resource = resourceLoader.getResource("classpath:mapremex.ldif");
            LdifReader ldifReader = new LdifReader(resource.getInputStream());
            ArrayList<LdifEntry> entries = new ArrayList<LdifEntry>();
            List<Organizacion> organizacionList = new ArrayList<>();
            for (LdifEntry entry : ldifReader) {
                try {
                    if (entry.getDn().getName().contains(ou[0])) {
                        organizacionList.add(Organizacion.builder()
                                .compania(entry.getEntry().get("company").get().getString())
                                .correo(entry.getEntry().get("mail").get().getString())
                                .departamento(entry.getEntry().get("department").get().getString())
                                .descripcion(entry.getEntry().get("description").get().getString())
                                .titulo(entry.getEntry().get("title").get().getString())
                                .dn(entry.getEntry().get("distinguishedname").get().getString())
                                .build());
                    }
                } catch (Exception e) {
                    System.out.println("Error:["+ ExceptionUtils.getFullStackTrace(e) +"]");
                }
            }
            if (organizacionList != null && organizacionList.size() > 0) {
                organizacionService.delete(organizacionService.findAll());
                organizacionService.insert(organizacionList);
            }
        } catch (Exception e) {
            LOGGER.error("Error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
        }
    }

    public void syncDirContext() {
        try {
            List<Organizacion> organizacionList = ldapReaderService.buildEntry();
            if (organizacionList != null && organizacionList.size() > 0) {
                organizacionService.delete(organizacionService.findAll());
                organizacionService.insert(organizacionList);
            }
        } catch (Exception e) {
            LOGGER.error("Error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
        }
    }

}
