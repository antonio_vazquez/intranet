package com.cie.intranet.config;

import com.cie.intranet.model.db.Usuario;
import com.cie.intranet.model.db.UsuarioPerfil;
import com.cie.intranet.services.db.UsuarioPerfilService;
import com.cie.intranet.services.db.UsuarioService;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.ldif.LdifEntry;
import org.apache.directory.api.ldap.model.ldif.LdifReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private ResourceLoader resourceLoader;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private UsuarioPerfilService usuarioPerfilService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        String name = authentication.getName();
        String password = authentication.getCredentials().toString();

        if (validator(name, password)) {
            // use the credentials
            // and authenticate against the third-party system
            Usuario usuario = usuarioService.findByCorreo(name);
            UsuarioPerfil usuarioPerfil = usuarioPerfilService.findByUsuarioId(usuario.getId());
            List<GrantedAuthority> setAuths = null;
            if (usuario != null && usuario.getId() > 0) {
                //usuarioService.update(usuario);
                setAuths = new ArrayList<GrantedAuthority>();

                setAuths.add(new SimpleGrantedAuthority(usuarioPerfil.getRol().getNombre()));
            }
            return new UsernamePasswordAuthenticationToken(
                    name, password, setAuths);
        } else {
            return null;
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

    private String discover(String user) throws LdapException, IOException {
        Resource resource = resourceLoader.getResource("classpath:mapremex.ldif");
        LdifReader ldifReader = new LdifReader(resource.getInputStream());
        ArrayList<LdifEntry> entries = new ArrayList<LdifEntry>();
        for (LdifEntry entry : ldifReader) {
            try {
                if (entry.getEntry().get("samaccountname").getString().contains(user)) {
                    return entry.getEntry().get("distinguishedname").getString();
                }
            } catch (Exception e) {
                System.out.println("Error:["+ ExceptionUtils.getFullStackTrace(e) +"]");
             }
        }
        //return entries;
        return null;
    }

    private boolean validator(String user, String password) {
        try {
            DirContext ctx = null;
            Hashtable env = new Hashtable(11);
            boolean b = false;
            try {

                env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
                env.put(Context.PROVIDER_URL, "ldap://172.16.64.31/DC=cieautomotive,DC=local");
                env.put(Context.SECURITY_AUTHENTICATION, "simple");
                env.put(Context.SECURITY_PRINCIPAL, discover(user));
                //env.put(Context.SECURITY_PRINCIPAL, "uid="+user+",OU=MATIC,DC=cieautomotive,DC=local");
                env.put(Context.SECURITY_CREDENTIALS, password);
                System.out.println("before context");
                ctx = new InitialDirContext(env);
                //The user is authenticated.
                b = true;
            } catch (NamingException e) {
                System.out.println("the user is not authenticated return false");
                b = false;
            }finally{
                if(ctx != null)
                    ctx.close();
            }
            return b;
        } catch (Exception e) {
            return false;
        }
    }
}
