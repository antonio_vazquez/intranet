package com.cie.intranet.repository;

import com.cie.intranet.model.db.Procedimiento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface ProcedimientoRepository extends JpaRepository<Procedimiento, Long>, QuerydslPredicateExecutor<Procedimiento> {


}
