package com.cie.intranet.repository;

import com.cie.intranet.model.db.Comunicado;
import com.cie.intranet.model.db.Planta;
import com.cie.intranet.model.db.Politica;
import com.cie.intranet.model.db.UsuarioPlanta;
import jdk.internal.dynalink.linker.LinkerServices;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;

public interface ComunicadoRepository extends JpaRepository<Comunicado, Long>, QuerydslPredicateExecutor<Comunicado> {

    Comunicado findByPlantaId(Long idPlanta);

    List<Comunicado> findAllByPlantaIn(List<Planta> plantaList);

    List<Comunicado> findAllByPlantaInAndId(List<Planta> plantaList, Long id);


}
