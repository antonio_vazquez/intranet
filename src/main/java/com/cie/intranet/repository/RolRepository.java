package com.cie.intranet.repository;

import com.cie.intranet.model.db.Rol;
import com.cie.intranet.model.db.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;

public interface RolRepository extends JpaRepository<Rol, Long>, QuerydslPredicateExecutor<Rol> {

    Rol findByNombreContains(String name);
}
