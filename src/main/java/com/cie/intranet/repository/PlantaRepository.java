package com.cie.intranet.repository;

import com.cie.intranet.model.db.Planta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;

public interface PlantaRepository extends JpaRepository<Planta, Long>, QuerydslPredicateExecutor<Planta> {



}
