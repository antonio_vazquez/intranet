package com.cie.intranet.repository;

import com.cie.intranet.model.db.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;

public interface UsuarioRepository extends JpaRepository<Usuario, Long>, QuerydslPredicateExecutor<Usuario> {

    List<Usuario> findAllByRol_Id (Long id);
    Usuario findByCorreo(String correo);

    List<Usuario> findAllByRol_IdOrRol_Id (Long id, Long id2);
}
