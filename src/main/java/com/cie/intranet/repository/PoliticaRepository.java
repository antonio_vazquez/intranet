package com.cie.intranet.repository;

import com.cie.intranet.model.db.Politica;
import com.cie.intranet.model.db.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;

public interface PoliticaRepository extends JpaRepository<Politica, Long>, QuerydslPredicateExecutor<Politica> {

    List<Politica> findAllByNormatividadId(Long id);

}
