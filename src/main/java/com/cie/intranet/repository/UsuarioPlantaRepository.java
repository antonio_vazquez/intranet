package com.cie.intranet.repository;

import com.cie.intranet.model.db.UsuarioPlanta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;

public interface UsuarioPlantaRepository extends JpaRepository<UsuarioPlanta, Long>, QuerydslPredicateExecutor<UsuarioPlanta> {

    List<UsuarioPlanta> findAllByUsuarioId (Long idUsuario);

    UsuarioPlanta findByUsuario_IdAndPlanta_Id (Long idUsuario, Long idPlanta);

    List<UsuarioPlanta> findAllByPlantaId (Long idPlanta);

}
