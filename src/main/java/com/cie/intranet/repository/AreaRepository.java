package com.cie.intranet.repository;

import com.cie.intranet.model.db.Area;
import com.cie.intranet.model.db.Rol;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface AreaRepository extends JpaRepository<Area, Long>, QuerydslPredicateExecutor<Area> {

}
