package com.cie.intranet.repository;

import com.cie.intranet.model.db.Area;
import com.cie.intranet.model.db.Subarea;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface SubareaRepository extends JpaRepository<Subarea, Long>, QuerydslPredicateExecutor<Subarea> {

}
