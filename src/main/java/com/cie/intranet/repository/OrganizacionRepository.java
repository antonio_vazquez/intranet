package com.cie.intranet.repository;

import com.cie.intranet.model.db.Organizacion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;

public interface OrganizacionRepository extends JpaRepository<Organizacion, Long>, QuerydslPredicateExecutor<Organizacion> {

    List<Organizacion> findAllByCompaniaContainsAndCompaniaIsNotNull(String planta);

    List<Organizacion> findAllByCompaniaContainsAndCompaniaIsNotNullAndCorreoIsNotNull(String planta);

}
