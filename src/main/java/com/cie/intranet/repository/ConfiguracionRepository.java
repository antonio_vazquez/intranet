package com.cie.intranet.repository;

import com.cie.intranet.model.db.Configuracion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface ConfiguracionRepository extends JpaRepository<Configuracion, Long>, QuerydslPredicateExecutor<Configuracion> {

    Configuracion findByParametro(String parametro);

}
