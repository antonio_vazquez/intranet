package com.cie.intranet.repository;

import com.cie.intranet.model.db.Usuario;
import com.cie.intranet.model.db.UsuarioPerfil;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;

public interface UsuarioPerfilRepository extends JpaRepository<UsuarioPerfil, Long>, QuerydslPredicateExecutor<UsuarioPerfil> {

    UsuarioPerfil findByUsuarioId (Long id);

    UsuarioPerfil findByUsuario_IdAndAndRol_Id(Long idUsuario, Long idRol);

}
