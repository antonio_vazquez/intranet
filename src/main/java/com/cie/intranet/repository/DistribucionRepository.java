package com.cie.intranet.repository;

import com.cie.intranet.model.db.Distribucion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface DistribucionRepository extends JpaRepository<Distribucion, Long>, QuerydslPredicateExecutor<Distribucion> {

    Distribucion findByComunicadoId(Long comunicadoId);

}
