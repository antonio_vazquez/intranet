package com.cie.intranet.repository;

import com.cie.intranet.model.db.Area;
import com.cie.intranet.model.db.Normatividad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface NormatividadRepository extends JpaRepository<Normatividad, Long>, QuerydslPredicateExecutor<Normatividad> {

}
