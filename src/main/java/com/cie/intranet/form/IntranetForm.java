package com.cie.intranet.form;

import com.cie.intranet.model.db.IntranetFile;

import javax.validation.constraints.NotNull;
import java.util.List;

public class IntranetForm {

    @NotNull(message = "campo requerido")
    private Long politica;

    private List<IntranetFile> file;

    public Long getPolitica() {
        return politica;
    }

    public void setPolitica(Long politica) {
        this.politica = politica;
    }

    public List<IntranetFile> getFile() {
        return file;
    }

    public void setFile(List<IntranetFile> file) {
        this.file = file;
    }
}
