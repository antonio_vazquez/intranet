package com.cie.intranet.form;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

public class ComunicadoForm {

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date desde;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date hasta;

    private Long planta;

    private String nombre;

    public Date getDesde() {
        return desde;
    }

    public void setDesde(Date desde) {
        this.desde = desde;
    }

    public Date getHasta() {
        return hasta;
    }

    public void setHasta(Date hasta) {
        this.hasta = hasta;
    }

    public Long getPlanta() {
        return planta;
    }

    public void setPlanta(Long planta) {
        this.planta = planta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
