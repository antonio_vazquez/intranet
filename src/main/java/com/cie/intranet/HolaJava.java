package intranet;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import java.util.Hashtable;

/** Clase de prueba
*
*@author Antonio Vazquez
*/

public class HolaJava{

	/**Devuelve un saludo de prueba
	*@param quien de prueba
	*@return cadea de saludo
	*/

	//static String username = "addresslist.matic@cieautomotive";
	//static String username = "rony";
	//static String password = "Pa33word!";
/*	static String username = "lfrias";
	static String password = "Gk6VFFl4c";	*/
	static String username = "addresslist.matic";
	static String password = "Pa33word!";

	public static void main(String[] args) throws Exception {
		DirContext ctx = null;
		Hashtable env = new Hashtable(11);
		boolean b = false;
		try {
			env.put(Context.INITIAL_CONTEXT_FACTORY,
					"com.sun.jndi.ldap.LdapCtxFactory");
			//Path:LDAP://10.20.0.35/CN=addresslist.matic,OU=MATIC,DC=cieautomotive,DC=local
			env.put(Context.PROVIDER_URL, "ldap://10.20.0.35/DC=cieautomotive,DC=local");
			env.put(Context.SECURITY_AUTHENTICATION, "simple");
			env.put(Context.SECURITY_PRINCIPAL, "CN="+username+",OU=MATIC,DC=cieautomotive,DC=local");
			env.put(Context.SECURITY_CREDENTIALS, password);
			System.out.println("before context");
			// If there isn't a naming exception then the user is authenticated. Return true
			ctx = new InitialDirContext(env);
			//The user is authenticated.
			b = true;
		} catch (NamingException e) {
			System.out.println("the user is not authenticated return false");
			b = false;
		}finally{
			if(ctx != null)
				ctx.close();
		}
	}
	
	public String saluda(String quien){

	return String.format("Hola, (en Java)", quien);
	}
}