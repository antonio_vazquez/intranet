package com.cie.intranet.controller;

import com.cie.intranet.model.db.Procedimiento;
import com.cie.intranet.repository.ProcedimientoRepository;
import com.cie.intranet.services.db.ProcedimientoService;
import com.cie.intranet.services.db.UsuarioPlantaService;
import com.cie.intranet.services.db.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.security.Principal;
import java.util.Date;

@Controller
@RequestMapping("/intranet/procedimiento")
public class ProcedimientoController {


    @Autowired
    private ProcedimientoRepository procedimientoRepository;

    @Autowired
    private ProcedimientoService procedimientoService;

    @Autowired
    private UsuarioService usuarioService;

    @RequestMapping
    public String index(Model model) throws Exception {
        model.addAttribute("entities", procedimientoService.findAll());
        return "procedimiento/index";
    }

    @RequestMapping("/create")
    public String create(Model model)  {
        model.addAttribute("form", new Procedimiento());
       return "procedimiento/create";
    }

    @PostMapping("/create")
    public String create(@Validated @ModelAttribute("form") Procedimiento procedimiento, final BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes, Principal principal, Authentication authentication) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("form", new Procedimiento());
            return "intranet/procedimiento/create";
        }
        if (procedimiento.getFile() != null && procedimiento.getFile().getSize() > 0) {

            if (!procedimiento.getFile().isEmpty()) {

                procedimiento.setFilename(procedimiento.getFile().getOriginalFilename());
                procedimiento.setRegister(new Date());
                try {
                    String[] pattern = procedimiento.getFilename().split("\\.");
                    procedimiento.setExtension(pattern[pattern.length -1].toUpperCase());
                    procedimiento.setPdf(procedimiento.getFile().getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                procedimiento.setUsuario(usuarioService.findByCorreo(principal.getName()));
                procedimiento = procedimientoRepository.save(procedimiento);
            }
        }else {
            procedimiento.setRegister(new Date());
            procedimiento.setUsuario(usuarioService.findByCorreo(principal.getName()));
            procedimiento = procedimientoRepository.save(procedimiento);
        }

        if (procedimiento !=  null && procedimiento.getId() > 0) {
            redirectAttributes.addFlashAttribute("SUCCESS_MESSAGE", "Procedimiento registrado correctamente");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "No se pudo registrar el procedimiento correctamente, por favor intente más tarde");
        }
        return "redirect:/intranet/procedimiento";
    }

    @GetMapping("/update/{id}")
    public String update(Model model, @PathVariable("id") Long id, HttpServletRequest request) {
            model.addAttribute("form", procedimientoRepository.findById(id));
            return "procedimiento/update";
    }

    @PostMapping("/update")
    public String intranetUpdate(Model model, @Validated @ModelAttribute("form") Procedimiento form, final BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "intranet/procedimiento/update";
        }

        if (form.getFile() != null && form.getFile().getSize() > 0) {

            if (!form.getFile().isEmpty()) {

                form.setFilename(form.getFile().getOriginalFilename());
                try {
                    String[] pattern = form.getFilename().split("\\.");
                    form.setExtension(pattern[pattern.length -1].toUpperCase());
                    form.setPdf(form.getFile().getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }

        Procedimiento update = procedimientoService.update(form);

        if (update !=  null && update.getId() > 0) {
            redirectAttributes.addFlashAttribute("UPDATE_MESSAGE", "Procedimiento actualizado correctamente");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "No se pudo registrar el procedimiento correctamente, por favor intente más tarde");
        }
        return "redirect:/intranet/procedimiento";
    }

    @RequestMapping("/delete/{id}")
    public String delete(Model model, @PathVariable("id") Long id, RedirectAttributes redirectAttributes) {
        procedimientoService.delete(id);
        redirectAttributes.addFlashAttribute("DELETE_MESSAGE", "¡Procedimiento eliminado!");
        return "redirect:/intranet/procedimiento";
    }

    @RequestMapping(value = "/mediapdf/{id}")
    public void report(@PathVariable("id") Long id, HttpServletResponse response) throws IOException {
        Procedimiento procedimiento = procedimientoService.findOne(id);
        byte[] report = procedimiento.getPdf();
        response.setContentType(content(procedimiento.getExtension()));
        response.setContentLength(report.length);
        response.setHeader("Content-Disposition", "attachment;filename=" + procedimiento.getFilename());
/*        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=users.xls");
        response.getWriter().write(report.toString());*/
        OutputStream out = response.getOutputStream();
        out.write(report);
    }

    public String content(String extension) {
        try {
            switch (extension) {
                case "PDF": return "application/pdf";
                case "XLSM": return "application/vnd.ms-excel.sheet.macroEnabled.12";
                case "XLSX": return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                case "XLSB": return "application/vnd.ms-excel.sheet.binary.macroEnabled.12";
                case "XLS": return "application/vnd.ms-excel";
                case "DOC": return "application/msword";
                case "DOCX": return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                case "DOCM": return "application/vnd.ms-word.document.macroEnabled.12";
                case "DOTM": return "application/vnd.ms-word.template.macroEnabled.12";
                case "PPT": return "application/vnd.ms-powerpoint";
                case "PPTM": return "application/vnd.ms-powerpoint.presentation.macroEnabled.12";
                case "POT": return "application/vnd.ms-powerpoint";
                case "PPTX": return "application/vnd.openxmlformats-officedocument.presentationml.presentation";
                case "JPG": return "image/jpeg";
                case "JPEG": return "image/jpeg";
                case "PNG": return "image/png";
                case "TIF": return "image/tiff";
                case "GIF": return "image/gif";
                case "DWG": return "image/vnd.dwg";
                case "DXF": return "image/vnd.dwg";
                case "VSD": return "application/vnd.visio";
                case "TXT": return "text/plain";
                case "CSV": return "text/csv";
                case "ZIP": return "application/zip";
                case "RAR": return "application/x-rar-compressed";
                case "XML": return "application/xml";
                default: return "application/pdf";
            }
        } catch (Exception e) {
            return "application/pdf";
        }
    }


}
