package com.cie.intranet.controller;

import com.cie.intranet.model.db.Usuario;
import com.cie.intranet.repository.RolRepository;
import com.cie.intranet.repository.UsuarioRepository;
import com.cie.intranet.services.db.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@Controller
@RequestMapping("/intranet/usuario")
public class UsuarioController {


    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private RolRepository rolRepository;

    @RequestMapping
    public String index(Model model) throws Exception {
        model.addAttribute("entities", usuarioService.findAll());
        return "usuario/index";
    }


    @RequestMapping("/create")
    public String create(Model model)  {
        model.addAttribute("form", new Usuario());
        model.addAttribute("rol", rolRepository.findAll() );
       return "usuario/create";
    }


    @PostMapping("/create")
    public String create(@Validated @ModelAttribute("form") Usuario usuario, final BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes, Principal principal) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("form", new Usuario());
            model.addAttribute("rol",rolRepository.findAll());
            return "intranet/usuario/create";
        }

        if (usuarioService.findByCorreo(usuario.getCorreo()) != null){
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "No se agrego registro, El usuario ya existe!");
            return "redirect:/intranet/usuario";
        }

        Usuario insert = usuarioRepository.save(usuario);

        if (insert !=  null && insert.getId() > 0) {
            redirectAttributes.addFlashAttribute("SUCCESS_MESSAGE", "Usuario registrado correctamente");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "No se pudo registrar correctamente, por favor intente más tarde");
        }
        return "redirect:/intranet/usuario";
    }

    @GetMapping("/update/{id}")
    public String update(Model model, @PathVariable("id") Long id, HttpServletRequest request) {
            model.addAttribute("form", usuarioRepository.findById(id));
            model.addAttribute("rol", rolRepository.findAll());
            return "usuario/update";
    }


    @PostMapping("/update")
    public String intranetUpdate(Model model, @Validated @ModelAttribute("form") Usuario form, final BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "intranet/usuario/update";
        }
        Usuario update = usuarioService.update(form);
        if (update !=  null && update.getId() > 0) {
            redirectAttributes.addFlashAttribute("UPDATE_MESSAGE", "Registro actualizado correctamente");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "No se pudo registrar correctamente, por favor intente más tarde");
        }
        return "redirect:/intranet/usuario";
    }



    @RequestMapping("/delete/{id}")
    public String delete(Model model, @PathVariable("id") Long id, RedirectAttributes redirectAttributes) {
        usuarioService.delete(id);
        redirectAttributes.addFlashAttribute("DELETE_MESSAGE", "Registro eliminado");
        return "redirect:/intranet/usuario";
    }




}
