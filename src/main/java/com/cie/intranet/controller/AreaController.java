package com.cie.intranet.controller;

import com.cie.intranet.model.db.Area;
import com.cie.intranet.repository.AreaRepository;
import com.cie.intranet.services.db.AreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@Controller
@RequestMapping("/intranet/area")
public class AreaController {

    @Autowired
    private AreaRepository areaRepository;

    @Autowired
    private AreaService areaService;

    @RequestMapping
    public String index(Model model) throws Exception {
        model.addAttribute("entities", areaRepository.findAll());
        return "area/index";
    }


    @RequestMapping("/create")
    public String create(Model model)  {
        model.addAttribute("form", new Area());
       return "area/create";
    }


    @PostMapping("/create")
    public String create(@Validated @ModelAttribute("form") Area area, final BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes, Principal principal) {
        if (bindingResult.hasErrors()) {
            return "intranet/area/create";
        }

        Area insert = areaRepository.save(area);

        if (insert !=  null && insert.getId() > 0) {
            redirectAttributes.addFlashAttribute("SUCCESS_MESSAGE", "Area registrada correctamente");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "No se pudo registrar correctamente, por favor intente más tarde");
        }
        return "redirect:/intranet/area";
    }

    @GetMapping("/update/{id}")
    public String update(Model model, @PathVariable("id") Long id, HttpServletRequest request) {
            model.addAttribute("form", areaRepository.findById(id));
            return "area/update";
    }


    @PostMapping("/update")
    public String intranetUpdate(Model model, @Validated @ModelAttribute("form") Area form, final BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "intranet/area/update";
        }
        Area update = areaService.update(form);
        if (update !=  null && update.getId() > 0) {
            redirectAttributes.addFlashAttribute("UPDATE_MESSAGE", "Registro actualizado correctamente");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "No se pudo registrar correctamente, por favor intente más tarde");
        }
        return "redirect:/intranet/area";
    }



    @RequestMapping("/delete/{id}")
    public String delete(Model model, @PathVariable("id") Long id, RedirectAttributes redirectAttributes) {
        areaService.delete(id);
        redirectAttributes.addFlashAttribute("DELETE_MESSAGE", "Registro eliminado");
        return "redirect:/intranet/area";
    }




}
