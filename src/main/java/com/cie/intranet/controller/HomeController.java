package com.cie.intranet.controller;

import com.cie.intranet.model.db.Comunicado;
import com.cie.intranet.model.db.Planta;
import com.cie.intranet.model.db.UsuarioPlanta;
import com.cie.intranet.services.db.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Controller
@RequestMapping("/")
public class HomeController {

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private PoliticaService politicaService;

    @Autowired
    private ComunicadoService comunicadoService;

    @Autowired
    private UsuarioPlantaService usuarioPlantaService;

    @Autowired
    private ProcedimientoService procedimientoService;

    @RequestMapping
    public String index(final Principal principal) {
        if (principal != null) {
            return "redirect:/common/";
        }
        return "redirect:login";
    }

    @GetMapping("login")
    public String login(final Principal principal) {
        if (principal != null) {
            return "redirect:/common/";
        }
        return "login";
    }

    @GetMapping("common")
    public String common(Model model, Principal principal) {

        List<Comunicado> comunicadoList = new ArrayList<>();
        List<UsuarioPlanta> usuarioPlantaList = usuarioPlantaService.findByUsuario(usuarioService.findByCorreo(principal.getName()).getId());
        List<Planta> plantaList = new ArrayList<>();
        usuarioPlantaList.forEach(p-> {
            plantaList.add(p.getPlanta());
        });
        comunicadoList = comunicadoService.findAllByPlanta(plantaList);


        Collections.sort(comunicadoList, new Comparator<Comunicado>() {
            @Override
            public int compare(Comunicado o1, Comunicado o2) {

                return o2.getRegister().compareTo(o1.getRegister());
            }
        });


        model.addAttribute("entities", politicaService.findAll());
        model.addAttribute("procedimiento", procedimientoService.findAll());
        model.addAttribute("comunicados", comunicadoList);
        return "common/index";
    }

}
