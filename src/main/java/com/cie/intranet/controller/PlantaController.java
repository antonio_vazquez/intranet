package com.cie.intranet.controller;

import com.cie.intranet.model.db.Planta;
import com.cie.intranet.model.db.Politica;
import com.cie.intranet.repository.PlantaRepository;
import com.cie.intranet.services.db.PlantaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.OutputStream;
import java.security.Principal;
import java.util.Date;

@Controller
@RequestMapping("/intranet/planta")
public class PlantaController {


    @Autowired
    private PlantaRepository plantaRepository;

    @Autowired
    private PlantaService plantaService;


    @RequestMapping
    public String index(Model model) throws Exception {
        model.addAttribute("entities", plantaService.findAll());
        return "planta/index";
    }


    @RequestMapping("/create")
    public String create(Model model)  {
        model.addAttribute("form", new Planta());
       return "planta/create";
    }


    @PostMapping("/create")
    public String create(@Validated @ModelAttribute("form") Planta planta, final BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes, Principal principal, Authentication authentication) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("form", new Planta());
            return "intranet/planta/create";
        }

                planta = plantaRepository.save(planta);


        if (planta !=  null && planta.getId() > 0) {
            redirectAttributes.addFlashAttribute("SUCCESS_MESSAGE", "Planta registrada correctamente");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "No se pudo registrar la planta correctamente, por favor intente más tarde");
        }
        return "redirect:/intranet/planta";
    }

    @GetMapping("/update/{id}")
    public String update(Model model, @PathVariable("id") Long id, HttpServletRequest request) {
            model.addAttribute("form", plantaRepository.findById(id));
            return "planta/update";
    }


    @PostMapping("/update")
    public String intranetUpdate(Model model, @Validated @ModelAttribute("form") Planta form, final BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "intranet/planta/update";
        }


        Planta update = plantaService.update(form);

        if (update !=  null && update.getId() > 0) {
            redirectAttributes.addFlashAttribute("UPDATE_MESSAGE", "Planta actualizada correctamente");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "No se pudo registrar la planta correctamente, por favor intente más tarde");
        }
        return "redirect:/intranet/planta";
    }



    @RequestMapping("/delete/{id}")
    public String delete(Model model, @PathVariable("id") Long id, RedirectAttributes redirectAttributes) {
        plantaService.delete(id);
        redirectAttributes.addFlashAttribute("DELETE_MESSAGE", "¡Planta eliminada!");
        return "redirect:/intranet/planta";
    }



}
