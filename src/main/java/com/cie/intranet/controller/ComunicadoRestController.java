package com.cie.intranet.controller;

import com.cie.intranet.model.db.Distribucion;
import com.cie.intranet.model.db.Organizacion;
import com.cie.intranet.services.db.DistribucionService;
import com.cie.intranet.services.db.OrganizacionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/mpmx/intranet/comunicado")
public class ComunicadoRestController {

    @Autowired
    private OrganizacionService organizacionService;

    @Autowired
    private DistribucionService distribucionService;

    @GetMapping("/organizacion")
    public List<Organizacion> getOrganizacion() throws Exception {
        return organizacionService.findAll();
    }

    @GetMapping("/distribucion/{comunicadoId}")
    public Distribucion getDistribucion(@PathVariable Long comunicadoId) {
        return distribucionService.findByComunicado(comunicadoId);
    }

    @PostMapping("/distribucion")
    public Distribucion getDistribucion(@RequestBody Distribucion distribucion) {
        return distribucionService.syncDistribucion(distribucion);
    }
}
