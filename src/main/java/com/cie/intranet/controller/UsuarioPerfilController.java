package com.cie.intranet.controller;

import com.cie.intranet.model.db.UsuarioPerfil;
import com.cie.intranet.repository.RolRepository;
import com.cie.intranet.repository.UsuarioPerfilRepository;
import com.cie.intranet.repository.UsuarioRepository;
import com.cie.intranet.services.db.UsuarioPerfilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@Controller
@RequestMapping("/intranet/usuarioperfil")
public class UsuarioPerfilController {


    @Autowired
    private UsuarioPerfilRepository usuarioPerfilRepository;

    @Autowired
    private UsuarioPerfilService usuarioPerfilService;

    @Autowired
    private RolRepository rolRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @RequestMapping
    public String index(Model model) throws Exception {
        model.addAttribute("entities", usuarioPerfilService.findAll());
        return "usuarioperfil/index";
    }


    @RequestMapping("/create")
    public String create(Model model)  {
        model.addAttribute("form", new UsuarioPerfil());
        model.addAttribute("usuario", usuarioRepository.findAll());
        model.addAttribute("rol", rolRepository.findAll() );
       return "usuarioperfil/create";
    }


    @PostMapping("/create")
    public String create(@Validated @ModelAttribute("form") UsuarioPerfil usuarioperfil, final BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes, Principal principal) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("form", new UsuarioPerfil());
            model.addAttribute("usuario", usuarioRepository.findAll());
            model.addAttribute("rol",rolRepository.findAll());
            return "intranet/usuarioperfil/create";
        }

        if (usuarioPerfilService.findByUsuarioId(usuarioperfil.getUsuario().getId()) != null){
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "No se pudo registrar correctamente, ya tiene un perfil");
            return "redirect:/intranet/usuarioperfil";
        }

        UsuarioPerfil insert = usuarioPerfilRepository.save(usuarioperfil);

        if (insert !=  null && insert.getId() > 0) {
            redirectAttributes.addFlashAttribute("SUCCESS_MESSAGE", "Asignacion de perfil registrado correctamente");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "No se pudo registrar correctamente, por favor intente más tarde");
        }
        return "redirect:/intranet/usuarioperfil";
    }

    @GetMapping("/update/{id}")
    public String update(Model model, @PathVariable("id") Long id, HttpServletRequest request) {
            model.addAttribute("form", usuarioPerfilRepository.findById(id));
            model.addAttribute("usuario", usuarioRepository.findAll());
            model.addAttribute("rol", rolRepository.findAll());
            return "usuarioperfil/update";
    }


    @PostMapping("/update")
    public String intranetUpdate(Model model, @Validated @ModelAttribute("form") UsuarioPerfil form, final BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "intranet/usuarioperfil/update";
        }
        UsuarioPerfil update = usuarioPerfilService.update(form);
        if (update !=  null && update.getId() > 0) {
            redirectAttributes.addFlashAttribute("UPDATE_MESSAGE", "Registro actualizado correctamente");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "No se pudo registrar correctamente, por favor intente más tarde");
        }
        return "redirect:/intranet/usuarioperfil";
    }



    @RequestMapping("/delete/{id}")
    public String delete(Model model, @PathVariable("id") Long id, RedirectAttributes redirectAttributes) {
        usuarioPerfilService.delete(id);
        redirectAttributes.addFlashAttribute("DELETE_MESSAGE", "Registro eliminado");
        return "redirect:/intranet/usuarioperfil";
    }




}
