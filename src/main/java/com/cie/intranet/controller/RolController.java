package com.cie.intranet.controller;

import com.cie.intranet.model.db.Rol;
import com.cie.intranet.repository.RolRepository;
import com.cie.intranet.services.db.RolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@Controller
@RequestMapping("/intranet/rol")
public class RolController {



    @Autowired
    private RolRepository rolRepository;

    @Autowired
    private RolService rolService;

    @RequestMapping
    public String index(Model model) throws Exception {
        model.addAttribute("entities", rolRepository.findAll());
        return "rol/index";
    }


    @RequestMapping("/create")
    public String create(Model model)  {
        model.addAttribute("form", new Rol());
       return "rol/create";
    }


    @PostMapping("/create")
    public String create(@Validated @ModelAttribute("form") Rol rol, final BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes, Principal principal) {
        if (bindingResult.hasErrors()) {
            return "intranet/rol/create";
        }

        if(rolRepository.findByNombreContains(rol.getNombre()) != null){
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "No se pudo registrar correctamente, perfil duplicado");
            return "redirect:/intranet/rol";
        }

        Rol insert = rolRepository.save(rol);

        if (insert !=  null && insert.getId() > 0) {
            redirectAttributes.addFlashAttribute("SUCCESS_MESSAGE", "Rol registrado correctamente");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "No se pudo registrar correctamente, por favor intente más tarde");
        }
        return "redirect:/intranet/rol";
    }

    @GetMapping("/update/{id}")
    public String update(Model model, @PathVariable("id") Long id, HttpServletRequest request) {
            model.addAttribute("form", rolRepository.findById(id));
            return "rol/update";
    }


    @PostMapping("/update")
    public String intranetUpdate(Model model, @Validated @ModelAttribute("form") Rol form, final BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "intranet/rol/update";
        }
        Rol update = rolService.update(form);
        if (update !=  null && update.getId() > 0) {
            redirectAttributes.addFlashAttribute("UPDATE_MESSAGE", "Registro actualizado correctamente");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "No se pudo registrar correctamente, por favor intente más tarde");
        }
        return "redirect:/intranet/rol";
    }



    @RequestMapping("/delete/{id}")
    public String delete(Model model, @PathVariable("id") Long id, RedirectAttributes redirectAttributes) {
        rolService.delete(id);
        redirectAttributes.addFlashAttribute("DELETE_MESSAGE", "Registro eliminado");
        return "redirect:/intranet/rol";
    }




}
