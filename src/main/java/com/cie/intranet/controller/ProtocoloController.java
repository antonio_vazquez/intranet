package com.cie.intranet.controller;

import com.cie.intranet.model.db.Normatividad;
import com.cie.intranet.model.db.Politica;
import com.cie.intranet.repository.PoliticaRepository;
import com.cie.intranet.services.db.AreaService;
import com.cie.intranet.services.db.NormatividadService;
import com.cie.intranet.services.db.PoliticaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.OutputStream;
import java.security.Principal;
import java.security.cert.CollectionCertStoreParameters;
import java.util.Date;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/intranet/protocolo")
public class ProtocoloController {


    @Autowired
    private PoliticaRepository politicaRepository;

    @Autowired
    private PoliticaService politicaService;

    @Autowired
    private AreaService areaService;

    @Autowired
    private NormatividadService normatividadService;

    @RequestMapping
    public String index(Model model) throws Exception {
        model.addAttribute("entities", politicaService.findAll().stream().filter(p -> p.getNormatividad().getId() == 4).collect(Collectors.toList()));
        return "protocolo/index";
    }

    @RequestMapping("/create")
    public String create(Model model)  {
        model.addAttribute("form", new Politica());
        model.addAttribute("area", areaService.findAll().stream().filter(p -> p.getNormatividad().getId() == 4).collect(Collectors.toList()));
       return "protocolo/create";
    }

    @PostMapping("/create")
    public String create(@Validated @ModelAttribute("form") Politica politica, final BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes, Principal principal, Authentication authentication) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("form", new Politica());
            model.addAttribute("area", areaService.findAll().stream().filter(p -> p.getNormatividad().getId() == 4).collect(Collectors.toList()));
            return "intranet/protocolo/create";
        }
        if (politica.getFile() != null && politica.getFile().getSize() > 0) {

            if (!politica.getFile().isEmpty()) {

                politica.setFilename(politica.getFile().getOriginalFilename());
                politica.setRegister(new Date());
                politica.setUsername(principal.getName() != null ? principal.getName() : "");
                try {
                    String[] pattern = politica.getFilename().split("\\.");
                    politica.setExtension(pattern[pattern.length -1].toUpperCase());
                    politica.setPdf(politica.getFile().getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Normatividad normatividad = normatividadService.findOne(4L);
                politica.setNormatividad(normatividad);
                politica = politicaRepository.save(politica);
            }
        }else {
            politica.setRegister(new Date());
            politica.setUsername(principal.getName() != null ? principal.getName(): "");
            Normatividad normatividad = normatividadService.findOne(4L);
            politica.setNormatividad(normatividad);
            politica = politicaRepository.save(politica);
        }

        if (politica !=  null && politica.getId() > 0) {
            redirectAttributes.addFlashAttribute("SUCCESS_MESSAGE", "Politica registrada correctamente");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "No se pudo registrar la politica correctamente, por favor intente más tarde");
        }
        return "redirect:/intranet/protocolo";
    }

    @GetMapping("/update/{id}")
    public String update(Model model, @PathVariable("id") Long id, HttpServletRequest request) {
            model.addAttribute("form", politicaRepository.findById(id));
            model.addAttribute("area", areaService.findAll().stream().filter(p -> p.getNormatividad().getId() == 4).collect(Collectors.toList()));
            return "protocolo/update";
    }

    @PostMapping("/update")
    public String intranetUpdate(Model model, @Validated @ModelAttribute("form") Politica form, final BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("area", areaService.findAll().stream().filter(p -> p.getNormatividad().getId() == 4).collect(Collectors.toList()));
            return "intranet/protocolo/update";
        }

        if (form.getFile() != null && form.getFile().getSize() > 0) {

            if (!form.getFile().isEmpty()) {

                form.setFilename(form.getFile().getOriginalFilename());
                try {
                    String[] pattern = form.getFilename().split("\\.");
                    form.setExtension(pattern[pattern.length -1].toUpperCase());
                    form.setPdf(form.getFile().getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }

        Politica update = politicaService.update(form);

        if (update !=  null && update.getId() > 0) {
            redirectAttributes.addFlashAttribute("UPDATE_MESSAGE", "Politica actualizada correctamente");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "No se pudo registrar la politica correctamente, por favor intente más tarde");
        }
        return "redirect:/intranet/protocolo";
    }

    @RequestMapping("/delete/{id}")
    public String delete(Model model, @PathVariable("id") Long id, RedirectAttributes redirectAttributes) {
        politicaService.delete(id);
        redirectAttributes.addFlashAttribute("DELETE_MESSAGE", "¡Politica eliminada!");
        return "redirect:/intranet/protocolo";
    }

    @RequestMapping(value = "/mediapdf/{id}")
    public void report(@PathVariable("id") Long id, HttpServletResponse response) throws IOException {
        Politica politica = politicaService.findOne(id);
        byte[] report = politica.getPdf();
        response.setContentType(content(politica.getExtension()));
        response.setContentLength(report.length);
        response.setHeader("Content-Disposition", "attachment;filename=" + politica.getFilename());
/*        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=users.xls");
        response.getWriter().write(report.toString());*/
        OutputStream out = response.getOutputStream();
        out.write(report);
    }

    public String content(String extension) {
        try {
            switch (extension) {
                case "PDF": return "application/pdf";
                case "XLSM": return "application/vnd.ms-excel.sheet.macroEnabled.12";
                case "XLSX": return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                case "XLSB": return "application/vnd.ms-excel.sheet.binary.macroEnabled.12";
                case "XLS": return "application/vnd.ms-excel";
                case "DOC": return "application/msword";
                case "DOCX": return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                case "DOCM": return "application/vnd.ms-word.document.macroEnabled.12";
                case "DOTM": return "application/vnd.ms-word.template.macroEnabled.12";
                case "PPT": return "application/vnd.ms-powerpoint";
                case "PPTM": return "application/vnd.ms-powerpoint.presentation.macroEnabled.12";
                case "POT": return "application/vnd.ms-powerpoint";
                case "PPTX": return "application/vnd.openxmlformats-officedocument.presentationml.presentation";
                case "JPG": return "image/jpeg";
                case "JPEG": return "image/jpeg";
                case "PNG": return "image/png";
                case "TIF": return "image/tiff";
                case "GIF": return "image/gif";
                case "DWG": return "image/vnd.dwg";
                case "DXF": return "image/vnd.dwg";
                case "VSD": return "application/vnd.visio";
                case "TXT": return "text/plain";
                case "CSV": return "text/csv";
                case "ZIP": return "application/zip";
                case "RAR": return "application/x-rar-compressed";
                case "XML": return "application/xml";
                default: return "application/pdf";
            }
        } catch (Exception e) {
            return "application/pdf";
        }
    }

/*    @RequestMapping(value = "/{id}")
    public ResponseEntity<byte[]> getPdf(@PathVariable("id") Long id)  throws IOException {
        byte[] pdf = politicaService.findOne(id).getPdf();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        String filename = "output.pdf";
        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(pdf, headers, HttpStatus.OK);
        return response;
    }*/

/*    @RequestMapping(value = "/{id}")
    public void getReporte(@PathVariable("id") Long id, HttpServletResponse response) throws IOException {
        try{
            byte[] report =  politicaService.findOne(id).getPdf();
            response.setContentType("application/pdf; charset=UTF-8");
            response.setContentLength(report.length);
            response.setHeader("Content-Disposition", "inline;filename=file.pdf");
            OutputStream out = response.getOutputStream();
            out.write(report);
        }catch (Exception e){

        }

    }*/

    @RequestMapping("/{id}")
    public String report(Model model, @PathVariable("id") Long id, RedirectAttributes redirectAttributes, HttpServletRequest request, HttpSession httpSession, HttpServletResponse response) {
            try {
                byte[] documentInBytes = politicaService.findOne(id).getPdf();
                response.setDateHeader("Expires", -1);
                response.setContentType("application/pdf");
                response.setContentLength(documentInBytes.length);
                response.getOutputStream().write(documentInBytes);
            } catch (Exception ioe) {
            } finally {
            }
            return null;
        }


}
