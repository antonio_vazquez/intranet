package com.cie.intranet.controller;

import com.cie.intranet.model.db.UsuarioPlanta;
import com.cie.intranet.repository.PlantaRepository;
import com.cie.intranet.repository.UsuarioPlantaRepository;
import com.cie.intranet.repository.UsuarioRepository;
import com.cie.intranet.services.db.UsuarioPlantaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@Controller
@RequestMapping("/intranet/usuarioplanta")
public class UsuarioPlantaController {


    @Autowired
    private UsuarioPlantaRepository usuarioPlantaRepository;

    @Autowired
    private UsuarioPlantaService usuarioPlantaService;

    @Autowired
    private PlantaRepository plantaRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @RequestMapping
    public String index(Model model) throws Exception {
        model.addAttribute("entities", usuarioPlantaService.findAll());
        return "usuarioplanta/index";
    }


    @RequestMapping("/create")
    public String create(Model model)  {
        model.addAttribute("form", new UsuarioPlanta());
        model.addAttribute("usuario", usuarioRepository.findAll());
        model.addAttribute("planta", plantaRepository.findAll() );
       return "usuarioplanta/create";
    }


    @PostMapping("/create")
    public String create(@Validated @ModelAttribute("form") UsuarioPlanta usuarioPlanta, final BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes, Principal principal) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("form", new UsuarioPlanta());
            model.addAttribute("usuario", usuarioRepository.findAll());
            model.addAttribute("planta",plantaRepository.findAll());
            return "intranet/usuarioplanta/create";
        }

        if (usuarioPlantaService.findByUsuarioIdAndPlantaId(usuarioPlanta.getUsuario().getId(), usuarioPlanta.getPlanta().getId()) != null){
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "No se pudo registrar correctamente, ya existe agregado usuario con planta!");
            return "redirect:/intranet/usuarioplanta";
        }

        UsuarioPlanta insert = usuarioPlantaRepository.save(usuarioPlanta);

        if (insert !=  null && insert.getId() > 0) {
            redirectAttributes.addFlashAttribute("SUCCESS_MESSAGE", "Planta registrado correctamente");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "No se pudo registrar correctamente, por favor intente más tarde");
        }
        return "redirect:/intranet/usuarioplanta";
    }

    @GetMapping("/update/{id}")
    public String update(Model model, @PathVariable("id") Long id, HttpServletRequest request) {
            model.addAttribute("form", usuarioPlantaRepository.findById(id));
            model.addAttribute("usuario", usuarioRepository.findAll());
            model.addAttribute("planta", plantaRepository.findAll());
            return "usuarioplanta/update";
    }


    @PostMapping("/update")
    public String intranetUpdate(Model model, @Validated @ModelAttribute("form") UsuarioPlanta form, final BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "intranet/usuarioplanta/update";
        }
        UsuarioPlanta update = usuarioPlantaService.update(form);
        if (update !=  null && update.getId() > 0) {
            redirectAttributes.addFlashAttribute("UPDATE_MESSAGE", "Registro actualizado correctamente");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "No se pudo registrar correctamente, por favor intente más tarde");
        }
        return "redirect:/intranet/usuarioplanta";
    }



    @RequestMapping("/delete/{id}")
    public String delete(Model model, @PathVariable("id") Long id, RedirectAttributes redirectAttributes) {
        usuarioPlantaService.delete(id);
        redirectAttributes.addFlashAttribute("DELETE_MESSAGE", "Registro eliminado");
        return "redirect:/intranet/usuarioplanta";
    }




}
