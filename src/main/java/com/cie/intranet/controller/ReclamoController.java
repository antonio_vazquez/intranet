package com.cie.intranet.controller;

import com.cie.intranet.model.db.Comunicado;
import com.cie.intranet.model.logic.IntranetMailService;
import com.cie.intranet.repository.ComunicadoRepository;
import com.cie.intranet.services.db.ComunicadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.security.Principal;
import java.util.Date;

@Controller
@RequestMapping("/intranet/reclamo")
public class ReclamoController {

    @Autowired
    private ComunicadoRepository comunicadoRepository;

    @Autowired
    private ComunicadoService comunicadoService;

    @Autowired
    private IntranetMailService intranetMailService;


    @RequestMapping
    public String index(Model model) throws Exception {

        return "reclamo/index";
    }


    @RequestMapping("/create")
    public String create(Model model)  {
        model.addAttribute("form", new Comunicado());
       return "comunicado/create";
    }


    @PostMapping("/create")
    public String create(@Validated @ModelAttribute("form") Comunicado comunicado, final BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes, Principal principal, Authentication authentication) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("form", new Comunicado());
            return "intranet/comunicado/create";
        }
        if (comunicado.getFile() != null && comunicado.getFile().getSize() > 0) {

            if (!comunicado.getFile().isEmpty()) {

                comunicado.setFilename(comunicado.getFile().getOriginalFilename());
                comunicado.setRegister(new Date());
                comunicado.setUsername(principal.getName() != null ? principal.getName() : "");
                try {
                    String[] pattern = comunicado.getFilename().split("\\.");
                    comunicado.setExtension(pattern[pattern.length -1].toUpperCase());
                    comunicado.setPdf(comunicado.getFile().getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                comunicado = comunicadoRepository.save(comunicado);
            }
        }else {
            comunicado.setRegister(new Date());
            comunicado.setUsername(principal.getName() != null ? principal.getName(): "");
            comunicado = comunicadoRepository.save(comunicado);
        }

        if (comunicado !=  null && comunicado.getId() > 0) {
            redirectAttributes.addFlashAttribute("SUCCESS_MESSAGE", "Comunicado registrado correctamente");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "No se pudo registrar el comunicado correctamente, por favor intente más tarde");
        }
        return "redirect:/intranet/comunicado";
    }

    @GetMapping("/update/{id}")
    public String update(Model model, @PathVariable("id") Long id, HttpServletRequest request) {
            model.addAttribute("form", comunicadoRepository.findById(id));
            return "comunicado/update";
    }


    @PostMapping("/update")
    public String intranetUpdate(Model model, @Validated @ModelAttribute("form") Comunicado form, final BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "intranet/comunicado/update";
        }

        if (form.getFile() != null && form.getFile().getSize() > 0) {

            if (!form.getFile().isEmpty()) {

                form.setFilename(form.getFile().getOriginalFilename());
                try {
                    form.setPdf(form.getFile().getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }

        Comunicado update = comunicadoService.update(form);

        if (update !=  null && update.getId() > 0) {
            redirectAttributes.addFlashAttribute("UPDATE_MESSAGE", "Comunidado actualizado correctamente");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "No se pudo registrar el comunicado correctamente, por favor intente más tarde");
        }
        return "redirect:/intranet/comunicado";
    }



    @RequestMapping("/delete/{id}")
    public String delete(Model model, @PathVariable("id") Long id, RedirectAttributes redirectAttributes) {
        comunicadoService.delete(id);
        redirectAttributes.addFlashAttribute("DELETE_MESSAGE", "¡Comunicado eliminado!");
        return "redirect:/intranet/comunicado";
    }

    @RequestMapping(value = "/mediapdf/{id}")
    public void report(@PathVariable("id") Long id, HttpServletResponse response) throws IOException {
        Comunicado comunicado = comunicadoService.findOne(id);
        byte[] report = comunicado.getPdf();
        response.setContentType(content(comunicado.getExtension()));
        response.setContentLength(report.length);
        response.setHeader("Content-Disposition", "attachment;filename=" + comunicado.getFilename());
/*        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=users.xls");
        response.getWriter().write(report.toString());*/
        OutputStream out = response.getOutputStream();
        out.write(report);
    }

    public String content(String extension) {
        try {
            switch (extension) {
                case "PDF": return "application/pdf";
                case "XLSM": return "application/vnd.ms-excel.sheet.macroEnabled.12";
                case "XLSX": return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                case "XLSB": return "application/vnd.ms-excel.sheet.binary.macroEnabled.12";
                case "XLS": return "application/vnd.ms-excel";
                case "DOC": return "application/msword";
                case "DOCX": return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                case "DOCM": return "application/vnd.ms-word.document.macroEnabled.12";
                case "DOTM": return "application/vnd.ms-word.template.macroEnabled.12";
                case "PPT": return "application/vnd.ms-powerpoint";
                case "PPTM": return "application/vnd.ms-powerpoint.presentation.macroEnabled.12";
                case "POT": return "application/vnd.ms-powerpoint";
                case "PPTX": return "application/vnd.openxmlformats-officedocument.presentationml.presentation";
                case "JPG": return "image/jpeg";
                case "JPEG": return "image/jpeg";
                case "PNG": return "image/png";
                case "TIF": return "image/tiff";
                case "GIF": return "image/gif";
                case "DWG": return "image/vnd.dwg";
                case "DXF": return "image/vnd.dwg";
                case "VSD": return "application/vnd.visio";
                case "TXT": return "text/plain";
                case "CSV": return "text/csv";
                case "ZIP": return "application/zip";
                case "RAR": return "application/x-rar-compressed";
                case "XML": return "application/xml";
                default: return "application/pdf";
            }
        } catch (Exception e) {
            return "application/pdf";
        }
    }

    @RequestMapping("/sendmail/{id}")
    public String sendmail(Model model, @PathVariable("id") Long id, Principal principal, RedirectAttributes redirectAttributes) {
        try{
            intranetMailService.sender(id);

        }catch (Exception e){

        }
        redirectAttributes.addFlashAttribute("SEND_MESSAGE", " Comunidado enviado correctamente por correo.");
        return "redirect:/intranet/comunicado";
    }


}
