package com.cie.intranet.controller;

import ch.qos.logback.classic.util.StatusViaSLF4JLoggerFactory;
import com.cie.intranet.form.ComunicadoForm;
import com.cie.intranet.model.db.Comunicado;
import com.cie.intranet.model.db.Organizacion;
import com.cie.intranet.model.db.Planta;
import com.cie.intranet.model.db.UsuarioPlanta;
import com.cie.intranet.model.logic.IntranetMailService;
import com.cie.intranet.repository.ComunicadoRepository;
import com.cie.intranet.services.db.*;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.security.Principal;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/intranet/comunicado")
public class ComunicadoController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ComunicadoController.class);

    @Autowired
    private ComunicadoRepository comunicadoRepository;

    @Autowired
    private ComunicadoService comunicadoService;

    @Autowired
    private IntranetMailService intranetMailService;

    @Autowired
    private PlantaService plantaService;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private UsuarioPlantaService usuarioPlantaService;

    @Autowired
    private OrganizacionService organizacionService;

    @RequestMapping
    public String index(Model model, Principal principal) throws Exception {

        List<Comunicado> comunicadoList = new ArrayList<>();
        List<UsuarioPlanta> usuarioPlantaList = usuarioPlantaService.findByUsuario(usuarioService.findByCorreo(principal.getName()).getId());
        List<Planta> plantaList = new ArrayList<>();
        usuarioPlantaList.forEach(p-> {
            plantaList.add(p.getPlanta());
        });
        comunicadoList = comunicadoService.findAllByPlanta(plantaList);


        Collections.sort(comunicadoList, new Comparator<Comunicado>() {
            @Override
            public int compare(Comunicado o1, Comunicado o2) {

                return o2.getRegister().compareTo(o1.getRegister());
            }
        });

/*        comunicadoService.findAll().stream().sorted(Comparator.comparing(Comunicado::getRegister).reversed()).collect(Collectors.toList())*/

        model.addAttribute("form", new ComunicadoForm());
        model.addAttribute("planta", plantaService.findAll());
        model.addAttribute("entities", null);
        return "comunicado/index";
    }

    @PostMapping
    public String index(Model model, @Validated @ModelAttribute("form") ComunicadoForm comunicadoForm, final BindingResult bindingResult, Principal principal) {
        if (!bindingResult.hasErrors()) {
            List<Planta> plantaList = new ArrayList<>();
            List<UsuarioPlanta> usuarioPlantaList = usuarioPlantaService.findByUsuario(usuarioService.findByCorreo(principal.getName()).getId());
            int index=0;
            for (UsuarioPlanta usuarioPlanta: usuarioPlantaList){
                plantaList.add(index, plantaService.findOne(usuarioPlanta.getPlanta().getId()));
                index++;
            }
            List<Comunicado> reclamoList = comunicadoService.queryBuilder(comunicadoForm);
            Set<Long> idPlant =  plantaList.stream().map(Planta::getId).collect(Collectors.toSet());
            List<Comunicado> reclamoList2 = reclamoList.stream().filter(e -> idPlant.contains(e.getPlanta().getId())).collect(Collectors.toList());
            model.addAttribute("form", new ComunicadoForm());
            model.addAttribute("planta", plantaList);
            model.addAttribute("entities", reclamoList2);
        }
        return "comunicado/index";
    }

    @GetMapping("/{id}")
    public String findComunicado(Model model, @PathVariable("id") Long id, HttpServletRequest request,Principal principal) {
        List<Comunicado> comunicadoList = new ArrayList<>();
        List<UsuarioPlanta> usuarioPlantaList = usuarioPlantaService.findByUsuario(usuarioService.findByCorreo(principal.getName()).getId());
        List<Planta> plantaList = new ArrayList<>();
        usuarioPlantaList.forEach(p-> {
            plantaList.add(p.getPlanta());
        });
        comunicadoList = comunicadoService.findAllByPlantaAndId(plantaList, id);


        Collections.sort(comunicadoList, new Comparator<Comunicado>() {
            @Override
            public int compare(Comunicado o1, Comunicado o2) {

                return o2.getRegister().compareTo(o1.getRegister());
            }
        });


        model.addAttribute("entities", comunicadoList);
        return "comunicado/index";
    }


    @RequestMapping("/create")
    public String create(Model model, Principal principal)  {
        List<Planta> plantaList = new ArrayList<>();
        List<UsuarioPlanta> usuarioPlantaList = usuarioPlantaService.findByUsuario(usuarioService.findByCorreo(principal.getName()).getId());
        int index=0;
        for (UsuarioPlanta usuarioPlanta: usuarioPlantaList){
            plantaList.add(index, plantaService.findOne(usuarioPlanta.getPlanta().getId()));
            index++;
        }
        model.addAttribute("form", new Comunicado());
        model.addAttribute("planta", plantaList);
       return "comunicado/create";
    }


    @PostMapping("/create")
    public String create(@Validated @ModelAttribute("form") Comunicado comunicado, final BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes, Principal principal, Authentication authentication) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("form", new Comunicado());
            return "intranet/comunicado/create";
        }
        if (comunicado.getFile() != null && comunicado.getFile().getSize() > 0) {

            if (!comunicado.getFile().isEmpty()) {

                comunicado.setFilename(comunicadoService.stripAccents(comunicado.getFile().getOriginalFilename()));
                comunicado.setRegister(new Date());
                comunicado.setUsername(principal.getName() != null ? principal.getName() : "");
                List<Organizacion> organizacionList = organizacionService.findAllByPlant(plantaService.findOne(comunicado.getPlanta().getId()).getNombre());
                String listaDistribucion="";
                for (Organizacion organizacion: organizacionList){
                    listaDistribucion += organizacion.getCorreo() +",";
                }
                comunicado.setLista(listaDistribucion);
                try {
                    String[] pattern = comunicado.getFilename().split("\\.");
                    comunicado.setExtension(pattern[pattern.length -1].toUpperCase());
                    comunicado.setPdf(comunicado.getFile().getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String listaD="";
                int contador=1;
                for (Organizacion organizacion: organizacionList){
                    if (organizacion.getCorreo() != null){
                        if (contador < organizacionList.size()){
                            listaD += organizacion.getCorreo() +",";
                        }else {
                            listaD += organizacion.getCorreo();
                        }
                    }
                    contador++;
                }
                comunicado.setLista(listaD);
                comunicado = comunicadoRepository.save(comunicado);
            }
        }else {
            comunicado.setRegister(new Date());
            comunicado.setUsername(principal.getName() != null ? principal.getName(): "");
            List<Organizacion> organizacionList = organizacionService.findAllByPlant(plantaService.findOne(comunicado.getPlanta().getId()).getNombre());
            String listaDistribucion="";
            int contador=1;
            for (Organizacion organizacion: organizacionList){
                if (organizacion.getCorreo() != null){
                    if (contador < organizacionList.size()){
                        listaDistribucion += organizacion.getCorreo() +",";
                    }else {
                        listaDistribucion += organizacion.getCorreo();
                    }
                }
                contador++;
            }
            comunicado.setLista(listaDistribucion);
            comunicado = comunicadoRepository.save(comunicado);
        }

        if (comunicado !=  null && comunicado.getId() > 0) {
            redirectAttributes.addFlashAttribute("SUCCESS_MESSAGE", "Comunicado registrado correctamente");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "No se pudo registrar el comunicado correctamente, por favor intente más tarde");
        }
        return "redirect:/intranet/comunicado";
    }

    @GetMapping("/update/{id}")
    public String update(Model model, @PathVariable("id") Long id, HttpServletRequest request) {
            model.addAttribute("form", comunicadoRepository.findById(id));
            return "comunicado/update";
    }


    @PostMapping("/update")
    public String intranetUpdate(Model model, @Validated @ModelAttribute("form") Comunicado form, final BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "intranet/comunicado/update";
        }

        if (form.getFile() != null && form.getFile().getSize() > 0) {

            if (!form.getFile().isEmpty()) {

                form.setFilename(comunicadoService.stripAccents(form.getFile().getOriginalFilename()));
                try {
                    form.setPdf(form.getFile().getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }

        Comunicado update = comunicadoService.update(form);

        if (update !=  null && update.getId() > 0) {
            redirectAttributes.addFlashAttribute("UPDATE_MESSAGE", "Comunidado actualizado correctamente");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "No se pudo registrar el comunicado correctamente, por favor intente más tarde");
        }
        return "redirect:/intranet/comunicado";
    }



    @RequestMapping("/delete/{id}")
    public String delete(Model model, @PathVariable("id") Long id, RedirectAttributes redirectAttributes) {
        comunicadoService.delete(id);
        redirectAttributes.addFlashAttribute("DELETE_MESSAGE", "¡Comunicado eliminado!");
        return "redirect:/intranet/comunicado";
    }

    @RequestMapping(value = "/mediapdf/{id}")
    public void report(@PathVariable("id") Long id, HttpServletResponse response) throws IOException {
        Comunicado comunicado = comunicadoService.findOne(id);
        byte[] report = comunicado.getPdf();
        response.setContentType(content(comunicado.getExtension()));
        response.setContentLength(report.length);
        response.setHeader("Content-Disposition", "attachment;filename=" + comunicado.getFilename());
/*        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=users.xls");
        response.getWriter().write(report.toString());*/
        OutputStream out = response.getOutputStream();
        out.write(report);
    }

    public String content(String extension) {
        try {
            switch (extension) {
                case "PDF": return "application/pdf";
                case "XLSM": return "application/vnd.ms-excel.sheet.macroEnabled.12";
                case "XLSX": return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                case "XLSB": return "application/vnd.ms-excel.sheet.binary.macroEnabled.12";
                case "XLS": return "application/vnd.ms-excel";
                case "DOC": return "application/msword";
                case "DOCX": return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                case "DOCM": return "application/vnd.ms-word.document.macroEnabled.12";
                case "DOTM": return "application/vnd.ms-word.template.macroEnabled.12";
                case "PPT": return "application/vnd.ms-powerpoint";
                case "PPTM": return "application/vnd.ms-powerpoint.presentation.macroEnabled.12";
                case "POT": return "application/vnd.ms-powerpoint";
                case "PPTX": return "application/vnd.openxmlformats-officedocument.presentationml.presentation";
                case "JPG": return "image/jpeg";
                case "JPEG": return "image/jpeg";
                case "PNG": return "image/png";
                case "TIF": return "image/tiff";
                case "GIF": return "image/gif";
                case "DWG": return "image/vnd.dwg";
                case "DXF": return "image/vnd.dwg";
                case "VSD": return "application/vnd.visio";
                case "TXT": return "text/plain";
                case "CSV": return "text/csv";
                case "ZIP": return "application/zip";
                case "RAR": return "application/x-rar-compressed";
                case "XML": return "application/xml";
                default: return "application/pdf";
            }
        } catch (Exception e) {
            return "application/pdf";
        }
    }

    @RequestMapping("/sendmail/{id}")
    public String sendmail(Model model, @PathVariable("id") Long id, Principal principal, RedirectAttributes redirectAttributes) {
        try{
            Comunicado comunicado = comunicadoService.findOne(id);
            comunicado.setEntregado(true);
            intranetMailService.sender(id);
            comunicadoService.updateEntrega(comunicado);

        }catch (Exception e){
            LOGGER.error("Ocurrio un error en el envio de correo: [" + ExceptionUtils.getFullStackTrace(e) +"]");
        }
        redirectAttributes.addFlashAttribute("SEND_MESSAGE", " Comunidado enviado correctamente por correo.");
        return "redirect:/intranet/comunicado";
    }


}
