package com.cie.intranet.controller;

import com.cie.intranet.services.db.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AdvisorController {

    @Autowired
    private UsuarioService usuarioService;

    @RequestMapping("/advisor")
    public String index(Model model) throws Exception {

        model.addAttribute("entities", usuarioService.findAll());
        return "index";
    }


}
