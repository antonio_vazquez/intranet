package com.cie.intranet.controller;

import com.cie.intranet.model.db.Distribucion;
import com.cie.intranet.model.db.Organizacion;
import com.cie.intranet.model.db.Politica;
import com.cie.intranet.services.db.DistribucionService;
import com.cie.intranet.services.db.OrganizacionService;
import com.cie.intranet.services.db.PoliticaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/mpmx/intranet/politica")
public class PoliticaRestController {

    @Autowired
    private OrganizacionService organizacionService;

    @Autowired
    private PoliticaService politicaService;

    @Autowired
    private DistribucionService distribucionService;

    @GetMapping("/politica")
    public List<Politica> getPolitica() throws Exception {
        return  politicaService.findAll();
    }


    @GetMapping("/distribucion/{comunicadoId}")
    public Distribucion getDistribucion(@PathVariable Long comunicadoId) {
        return distribucionService.findByComunicado(comunicadoId);
    }

    @PostMapping("/distribucion")
    public Distribucion getDistribucion(@RequestBody Distribucion distribucion) {
        return distribucionService.syncDistribucion(distribucion);
    }
}
